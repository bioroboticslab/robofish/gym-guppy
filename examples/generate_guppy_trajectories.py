"""Generate guppy trajectories"""

import argparse
from pathlib import Path
import logging

import numpy as np
import robofish.gym_guppy.envs
import robofish.gym_guppy.guppies
import robofish.io
from tqdm import trange


class Filter(logging.Filter):
    """
    Filter to silence the warning coming from robofish.io.

    The warning normally makes sense to avoid that anyone passes orientations
    in degrees. In this case however we know that we are passing unbounded radians,
    which is ok.
    """

    def filter(self, record: logging.LogRecord) -> bool:
        return (
            not "Converting orientations, from a bigger range than [0, 2 * pi]. "
            "When passing the orientations, they are assumed to be in radians."
            in record.getMessage()
        )


def generate(
    output_path: Path,
    num_guppies: int = 2,
    num_tracks: int = 100,
    track_length_s: int = 600,
    guppy_type: str = "BoostCouzinGuppy",
    guppy_model: str = "",
) -> None:
    time_step_s = 0.04
    num_sim_steps = int(track_length_s / time_step_s)

    if guppy_model:
        import fish_models

        name = guppy_model
        guppy = fish_models.ModelGuppy
        guppy_args = {
            "model": guppy_model,
            "world_conversion_factor": 100,
            "frequency": 25,
            "vault": None,
        }
    else:
        guppy = name = guppy_type
        guppy_args = {}

    output_path = output_path / f"{name}_trajectories"
    output_path.mkdir(exist_ok=True, parents=True)

    env = robofish.gym_guppy.envs.ConfigurableGuppyEnv(
        num_guppies=num_guppies,
        guppy_type=guppy,
        robot_type=None,
        time_step_s=time_step_s,
        guppy_args=guppy_args,
    )

    for track_index in trange(num_tracks):
        env.reset()
        guppy_poses = []
        time_s = []
        for _ in range(num_sim_steps):
            env.step(None)
            guppy_poses.append([g.get_pose() for g in env.guppies])
            time_s.append(env.time)

        poses = np.array(guppy_poses)
        poses[:, :, :2] *= 100  # m to cm

        with robofish.io.File(
            output_path / f"{name}_{track_index:04}.hdf5",
            mode="w",
            monotonic_time_points_us=np.array(time_s) * 1e6,
            frequency_hz=25,
            world_size_cm=[100, 100],
        ) as f:
            for i in range(poses.shape[1]):
                guppy = f.create_entity(poses=poses[:, i], category=guppy_type)
                try:
                    guppy.attrs["zor"] = list(env.guppies)[i].zor * 100  # m to cm
                    guppy.attrs["zoo"] = list(env.guppies)[i].zoo * 100  # m to cm
                    guppy.attrs["zoa"] = list(env.guppies)[i].zoa * 100  # m to cm
                except AttributeError:
                    pass


if __name__ == "__main__":
    logging.getLogger().addFilter(Filter())
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "--num_guppies", type=int, default=2, help="number of guppies per trajectory"
    )
    parser.add_argument(
        "--num_tracks",
        type=int,
        default=100,
        help="number of trajectories/files to generate",
    )
    parser.add_argument(
        "--track_length_s",
        type=int,
        default=120,
        help="length of each trajectory in seconds",
    )
    parser.add_argument(
        "--guppy_type",
        type=str,
        default="BoostCouzinGuppy",
        help="Type of guppy, e.g. ClassicCouzinGuppy, BoostCouzinGuppy, ModelGuppy",
    )
    parser.add_argument(
        "--guppy_model",
        type=str,
        default="",
        help="Model for ModelGuppy. If the model is set, "
        "guppy_type is ignored and ModelGuppy is used.",
    )
    parser.add_argument(
        "output_path",
        type=str,
        help="Path where to store the trajectories",
    )
    args = parser.parse_args()
    generate(
        output_path=Path(args.output_path),
        num_guppies=args.num_guppies,
        num_tracks=args.num_tracks,
        track_length_s=args.track_length_s,
        guppy_type=args.guppy_type,
        guppy_model=args.guppy_model,
    )
