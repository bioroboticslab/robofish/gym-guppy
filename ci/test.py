#! /usr/bin/env python3
# SPDX-License-Identifier: LGPL-3.0-or-later

from os import environ as env
from subprocess import check_call
from pathlib import Path
from platform import system
from shutil import which


def python_executable():
    if system() == "Windows":
        return f"/Python{''.join(env['PYTHON_VERSION'].split('.'))}/python.exe"
    elif system() == "Linux" or system() == "Darwin":
        return which(f"python{env['PYTHON_VERSION']}")
    assert False


def python_venv_executable():
    if system() == "Windows":
        return str(Path(".venv/Scripts/python.exe").resolve())
    elif system() == "Linux" or system() == "Darwin":
        return str(Path(".venv/bin/python").resolve())
    assert False


if __name__ == "__main__":
    check_call(
        [
            python_executable(),
            "-m",
            "venv",
            "--system-site-packages",
            "--prompt",
            "ci",
            ".venv",
        ]
    )

    check_call(
        [
            python_venv_executable(),
            "-m",
            "pip",
            "install",
            "--extra-index-url",
            "https://git.imp.fu-berlin.de/api/v4/projects/6392/packages/pypi/simple",
            str(sorted(Path("dist").glob("*.whl"))[-1].resolve()),
        ]
    )

    check_call(
        [
            python_venv_executable(),
            "-m",
            "pip",
            "install",
            "pytest",
            "pytest-cov",
        ]
    )

    check_call(
        [
            python_venv_executable(),
            "-m",
            "pytest",
            "--cov=robofish.gym_guppy",
            "--cov-report=term-missing",
        ]
    )
