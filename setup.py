from subprocess import run, PIPE

from setuptools import setup, find_packages


def source_version():
    version_parts = (
        run(
            ["git", "describe", "--tags", "--dirty"],
            check=True,
            stdout=PIPE,
            encoding="utf-8",
        )
        .stdout.strip()
        .split("-")
    )

    if version_parts[-1] == "dirty":
        dirty = True
        version_parts = version_parts[:-1]
    else:
        dirty = False

    version = version_parts[0]
    if len(version_parts) == 3:
        version += ".post0"
        version += f".dev{version_parts[1]}+{version_parts[2]}"
    if dirty:
        version += "+dirty"

    return version


setup(
    name="robofish-gym-guppy",
    version=source_version(),
    install_requires=[
        "gymnasium[box2d]",
        "numpy",
        "scipy",
        "numba",
        "h5py",
        "robofish-core",
        "deprecated",
    ],
    extras_require={
        "all": [
            "matplotlib",
            "pygame",
            "imageio",
            "imageio-ffmpeg",
        ],
    },
    package_dir={"": "src"},
    packages=[f"robofish.{p}" for p in find_packages("src/robofish")],
    zip_safe=True,
)
