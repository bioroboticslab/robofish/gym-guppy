# Changelog

## [0.5.0] - 2023-04-12

### Changed

- Switch from OpenAI gym to gymnasium and the new API

### Removed

- Removed support for old gym API

## [0.4.0] - 2023-03-24 

### Added

- Added more documentation

### Changed

- `guppy_action_frequency` is no longer required when there is no guppy
- Reward functions are now allowed to start with a minus sign
- Deprecated rendering of environments (use `robofish-io-render` instead)
- Deprecated `VariableStepGuppyEnv` (use `ConfigurableGuppyEnv` instead, do not use variable step length.)
- Deprecated `AdaptiveAgent` and related functions (unmaintained and untested)
- Deprecated built-in Couzin guppies (use `LiveFemaleFemaleConstantSpeedCouzinModel` from `fish_models` instead)
- Providing `controller_params` is now obligatory for robots that use PID controller simulation

## [0.3.0] - 2022-10-14

### Added

- Multi-robot support
- Support for guppies that need to set their own initial pose

### Removed

- Removed MXNetGuppy

### Changed

- Fixed `FrameStack`

## [0.2.0] - 2022-04-07

### Added

- Added more tests and documentation
- Added `done_when_goal_reached` flag to GoalGuppyEnv

### Changed

- Fixed info dict
- Allow using multiple guppy types in one env

## [0.1.0] - 2021-11-09

### Added

- Added TurnRobot
- Added DummyGuppy

### Changed

- Deprecated MXNetGuppy
- Fixed various small bugs
