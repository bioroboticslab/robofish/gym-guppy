import numpy as np
from robofish.gym_guppy.envs import ConfigurableGuppyEnv
from robofish.gym_guppy.wrappers.action_wrapper import (
    MovementLimitWrapper,
    FlatActionWrapper,
    ClipToActionSpaceWrapper,
)
from robofish.gym_guppy.wrappers.observation_wrapper import (
    FlatObservationsWrapper,
    FrameStack,
    IgnorePastWallsWrapper,
    RayCastingWrapper,
)


def create_env():
    """Create a dummy env for testing."""
    return ConfigurableGuppyEnv(guppy_type="DummyGuppy", robot_type="TurnSpeedRobot")


def test_movement_limit_wrapper():
    """Test that the movement limit wrapper works as expected."""
    env = create_env()
    env.reset()
    turn_limit = 0
    speed_limit = 0
    env = FlatActionWrapper(env)
    env = MovementLimitWrapper(env, turn_limit, speed_limit)
    clipped_action = env.action([1, 1])
    assert clipped_action.all() == 0


def test_clip_to_action_space_wrapper():
    """Test that the clip to action space wrapper works as expected."""
    env = create_env()
    env.reset()
    env = FlatActionWrapper(env)
    env = ClipToActionSpaceWrapper(env)

    for action in [[0, 0], [-10, -10], [10, 10]]:
        assert env.action_space.contains(env.action(np.float32(action)))


def test_flat_observation_wrapper():
    """Test that the flat observation wrapper works as expected."""
    env = create_env()
    obs, _ = env.reset()
    env = FlatObservationsWrapper(env)
    assert obs.flatten().shape == env.observation(obs).shape


def test_some_observation_wrappers():
    """Test that the some observation wrappers work as expected in combination."""
    env = create_env()
    obs, _ = env.reset()
    num_bins = 72
    env = RayCastingWrapper(env, num_bins=num_bins)
    obs_rc, _ = env.reset()
    k = 2
    env = FrameStack(env, k=k)
    obs_fs, _ = env.reset()
    env = IgnorePastWallsWrapper(env, k)
    obs_iw, _ = env.reset()

    assert obs_rc.shape[-1] == num_bins
    # check that it adds another dimension, but does not change the shape otherwise
    assert (k, *obs_rc.shape) == obs_fs.shape
    # IgnorePastWallsWrapper should reduce obs dimensionality back to 2
    assert len(obs_iw.shape) == 2
    assert obs_iw.shape[0] > obs.shape[0]
    assert obs_rc.shape[-1] == obs_fs.shape[-1] == obs_iw.shape[-1]
