"""Test the envs."""
import warnings
import pytest
import numpy as np
import gymnasium as gym
from gymnasium.utils.env_checker import check_env

from robofish.gym_guppy.envs import (
    GuppyEnv,
    ConfigurableGuppyEnv,
    GoalGuppyEnv,
    VariableStepGuppyEnv,
    VariableStepGoalGuppyEnv,
)

from robofish.gym_guppy.guppies import (
    DummyGuppy,
    TurnSpeedRobot,
    AdaptiveCouzinGuppy,
)


ENVS = (ConfigurableGuppyEnv,)
ABSTRACT_ENVS = (
    GuppyEnv,
    GoalGuppyEnv,
    VariableStepGuppyEnv,
    VariableStepGoalGuppyEnv,
)

DEPRECATED_ENVS = (VariableStepGoalGuppyEnv, VariableStepGuppyEnv)


@pytest.mark.parametrize("env_class", ABSTRACT_ENVS)
def test_abstract_envs(env_class):
    """Test that abstract envs by creating subclasses."""
    class Env(env_class):
        def _reset(self):
            self._add_guppy(
                DummyGuppy(
                    world=self.world,
                    world_bounds=self.world_bounds,
                    position=[0.1, 0.0],
                    orientation=0.0,
                )
            )
            self._add_robot(
                TurnSpeedRobot(
                    world=self.world,
                    world_bounds=self.world_bounds,
                    position=[0.0, 0.0],
                    orientation=0.0,
                )
            )

        def _update_goal(self):
            # needed by GoalGuppyEnv
            pass

        @property
        def desired_goal(self):
            # needed by GoalGuppyEnv
            return np.zeros((2,))

    if env_class in DEPRECATED_ENVS:
        with pytest.warns(DeprecationWarning):
            env = Env()
    else:
        env = Env()
    env.reset()
    env.step(action=np.array([0.0, 0.0]))


@pytest.mark.parametrize("env_class", ENVS)
def test_envs(env_class):
    """Test the most basic functionality of envs."""
    env = env_class(guppy_type=DummyGuppy, robot_type=TurnSpeedRobot)
    env.reset()
    state_t, *_ = env.step(action=np.array([0.0, 0.0]))
    assert state_t.dtype == np.float32


@pytest.mark.parametrize("env_class", ENVS)
def test_api_conformity(env_class):
    """Test that the env conforms to the openAI gym API."""
    if env_class in DEPRECATED_ENVS:
        with pytest.warns(DeprecationWarning):
            env = env_class()
    elif env_class == ConfigurableGuppyEnv:
        env = env_class(guppy_type=DummyGuppy, robot_type=TurnSpeedRobot)
    else:
        env = env_class()
    with pytest.warns(UserWarning) as warnings_checker:
        check_env(env, skip_render_check=True)
    for w in warnings_checker:
        print(dir(w.message))
        if (
            env_class == ConfigurableGuppyEnv
            and "WARN: A Box observation space" in str(w.message)  # new gym version
            or "WARN: Agent's m" in str(w.message)  # old gym version
            or "WARN: Your observation" in str(w.message)  # old gym version
        ):
            # The warnings are about the unwrapped observation space.
            # The unwrapped observation space is never really used and
            # transformed by wrappers.
            continue
        else:
            warnings.warn_explicit(
                message=w.message,
                category=w.category,
                filename=w.filename,
                lineno=w.lineno,
            )


@pytest.mark.parametrize("env_class", ENVS)
def test_rng(env_class):
    """Test that the env's random number generator is seeded correctly."""
    if gym.__version__ < "0.22":
        return
    obs = []
    for seed in range(2):
        obs.append([])
        for i in range(2):
            o, _ = env_class(guppy_type="DummyGuppy", robot_type=TurnSpeedRobot).reset(seed=seed)
            obs[seed].append(o)
    for o in obs:
        # initial observation shold be the same if seed is the same
        assert (o[0] == o[1]).all()
    # if seed is different, initial observation is very unlikely to be the same
    assert (obs[0][0] != obs[1][0]).any()


def test_multiple_guppy_configs():
    """Test using multiple guppy configs."""
    with pytest.warns(DeprecationWarning):
        env = ConfigurableGuppyEnv(
            guppy_type=AdaptiveCouzinGuppy,
            guppy_args=[{"zone_radius_mean": 0.1}, {"zone_radius_mean": 0.2}],
            num_guppies=2,
            robot_type=TurnSpeedRobot,
        )
    assert list(env.guppies)[0]._zone_radius_mean == 0.1
    assert list(env.guppies)[1]._zone_radius_mean == 0.2


def test_no_guppies():
    """Test that no guppy is added if num_guppies is 0."""
    # no need to pass guppy_action_frequency_hz
    env = ConfigurableGuppyEnv(
        robot_action_frequency_hz=1.0,
        robot_type=TurnSpeedRobot,
        num_guppies=0,
        guppy_type=DummyGuppy,
    )
    assert not env.num_guppies


def test_guppy_action_frequency_hz():
    """Test that guppy_action_frequency_hz is required if there is a guppy."""
    ConfigurableGuppyEnv(
        robot_action_frequency_hz=1.0,
        guppy_action_frequency_hz=2.0,
        num_guppies=0,
        guppy_type=DummyGuppy,
        robot_type=TurnSpeedRobot,
    )
    with pytest.raises(ValueError):
        # ValueError if guppy_action_frequency_hz is not passed if there is a guppy
        ConfigurableGuppyEnv(
            robot_action_frequency_hz=1.0,
            num_guppies=1,
            guppy_type=DummyGuppy,
        )


def test_no_robot():
    """Test that no robot is added if num_robots is 0."""
    env = ConfigurableGuppyEnv(
        robot_action_frequency_hz=1.0,
        guppy_action_frequency_hz=2.0,
        num_robots=0,
        guppy_type=DummyGuppy,
        robot_type=TurnSpeedRobot,
    )
    assert not env.num_robots
