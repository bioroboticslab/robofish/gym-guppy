from robofish.gym_guppy.envs import GuppyEnv, ConfigurableGuppyEnv
from robofish.gym_guppy.guppies import (
    TurnSpeedAgent,
    PerturbedAdaptiveCouzinGuppy,
    TorqueGuppy,
    BoostGuppy,
    RandomTurnBoostGuppy,
    AdaptiveCouzinGuppy,
    BiasedAdaptiveCouzinGuppy,
    BoostCouzinGuppy,
    ClassicCouzinGuppy,
)
import numpy as np
from Box2D import b2World
import pytest

# excluding the abstract ones
BASIC_GUPPIES = (
    TorqueGuppy,
    BoostGuppy,
    RandomTurnBoostGuppy,
)
COUZIN_GUPPIES = (
    AdaptiveCouzinGuppy,
    BiasedAdaptiveCouzinGuppy,
    BoostCouzinGuppy,
    ClassicCouzinGuppy,
    PerturbedAdaptiveCouzinGuppy,
)


def test_agent_without_env():
    """Test that agents can be created without an env and do not cause errors."""
    world_bounds = (
        np.array([-GuppyEnv.world_width / 2, -GuppyEnv.world_height / 2]),
        np.array([GuppyEnv.world_width / 2, GuppyEnv.world_height / 2]),
    )
    world = b2World(gravity=(0, 0), doSleep=True)
    TurnSpeedAgent(world=world, world_bounds=world_bounds)


@pytest.mark.parametrize("guppy", BASIC_GUPPIES + COUZIN_GUPPIES)
def test_guppies(guppy):
    """Test that guppies can be created and do not cause errors."""
    with pytest.warns(DeprecationWarning):
        env = ConfigurableGuppyEnv(guppy_type=guppy, robot_type="TurnSpeedRobot")
        env.reset()
    env.step([0.0, 0.0])


@pytest.mark.parametrize("guppy", BASIC_GUPPIES + COUZIN_GUPPIES)
def test_guppies_without_rng(guppy, capsys):
    """Test that guppies raise an error if they are not given a rng."""
    with pytest.raises(ValueError):
        with pytest.warns(DeprecationWarning):
            ConfigurableGuppyEnv(
                guppy_type=guppy,
                guppy_pose_rng="let_guppies_set_init_pose",
                robot_type="TurnSpeedRobot",
            )
