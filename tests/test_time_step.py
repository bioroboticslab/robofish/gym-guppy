"""Test various values for `time_step_s`."""""
import pytest
import numpy as np

from robofish.gym_guppy.envs import ConfigurableGuppyEnv
from robofish.gym_guppy.guppies import DummyGuppy, TurnSpeedRobot


def ts_env(time_step_s):
    """Create a ConfigurableGuppyEnv with a time step of time_step_s.

    Args:
        time_step_s (float): The time step in seconds.

    Returns:
        env (ConfigurableGuppyEnv): The environment."""
    return ConfigurableGuppyEnv(
        robot_type=TurnSpeedRobot,
        guppy_type=DummyGuppy,
        robot_pose_rng=lambda *args, **kwargs: np.zeros(3),
        guppy_pose_rng=lambda *args, **kwargs: np.zeros((1, 3)),
        time_step_s=time_step_s,
    )


@pytest.mark.parametrize("time_step_s", [0.01, 0.02, 0.04, 0.05, 0.1, 1.0])
def test_time_step_s(time_step_s):
    """Test that an environment can be created with various values for `time_step_s`."""
    ts_env(time_step_s)


@pytest.mark.parametrize("time_step_s", [0.001, 0.123, 1.234])
def test_time_step_s_warning(time_step_s):
    """Test that a warning is raised when `time_step_s` is not a multiple of 0.01."""
    with pytest.raises(ValueError):
        ts_env(time_step_s)
