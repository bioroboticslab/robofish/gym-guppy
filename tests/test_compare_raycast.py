"""
The old raycasting implementation has been replaced with the
implementation provided by robofish.core.  The tests in this file compare the
results of the new implementation with the old results.  The old implementation
has been copied to old_raycasts.py.
"""
import numpy as np
import old_raycast
from robofish.gym_guppy.tools.math import (
    rad_to_orientation_vector,
    compute_dist_bins,
    ray_casting_walls,
)
from robofish.core import (
    entity_boundary_distance,
    entity_entities_minimum_sector_distances,
)


def sample(a, b):
    return (b - a) * np.random.random() + a


def raycast_walls(observer_pose, boundary, ray_angles, far_plane):
    kwargs = dict(
        fish_pose=observer_pose,
        world_bounds=boundary,
        ray_orientations=ray_angles,
    )
    raycast_core = ray_casting_walls(**kwargs, max_dist=far_plane)
    raycast_old_gym = old_raycast.ray_casting_walls(**kwargs, diagonal_length=far_plane)
    return raycast_core, raycast_old_gym


def random_diffs_walls(n):
    for i in range(n):
        wall_len = sample(0.01, 200.0)
        boundary = np.float32(
            [[-wall_len / 2, -wall_len / 2], [wall_len / 2, wall_len / 2]]
        )
        far_plane = np.sqrt(2 * wall_len ** 2)

        observer_pose = np.float32(
            [
                sample(boundary[0][0], boundary[1][0]),
                sample(boundary[0][1], boundary[1][1]),
                sample(-np.pi, np.pi),
            ]
        )
        opening = sample(0.05, 1)
        n_rays = np.random.randint(low=1, high=128)
        ray_angles = np.linspace(-np.pi * opening, np.pi * opening, n_rays)
        raycasts = raycast_walls(
            observer_pose=observer_pose,
            boundary=boundary,
            ray_angles=ray_angles,
            far_plane=far_plane,
        )
        # mostly the results are equal to at least 4 decimals,
        # but tests get flaky.
        np.testing.assert_almost_equal(*raycasts, decimal=3)


def raycast_entities(entity_poses, sector_limit_angles, far_plane):
    kwargs = dict(
        relative_to=entity_poses[0],
        poses=entity_poses[1:],
        bin_boundaries=sector_limit_angles,
        max_dist=far_plane,
    )
    raycast_core = compute_dist_bins(**kwargs)
    raycast_old_gym = old_raycast.compute_dist_bins(**kwargs)
    return raycast_core, raycast_old_gym


def random_diffs_entities(n):
    for i in range(n):
        wall_len = sample(0.01, 200.0)
        boundary = np.float32(
            [[-wall_len / 2, -wall_len / 2], [wall_len / 2, wall_len / 2]]
        )
        far_plane = np.sqrt(2 * wall_len ** 2)
        n_entities = np.random.randint(1, 100)
        entitiy_poses = np.stack(
            [
                np.float32(
                    [
                        sample(boundary[0][0], boundary[1][0]),
                        sample(boundary[0][1], boundary[1][1]),
                        sample(-np.pi, np.pi),
                    ]
                )
                for _ in range(n_entities)
            ]
        )
        opening = sample(0.05, 1)
        n_rays = np.random.randint(low=1, high=128)
        sector_limit_angles = np.linspace(-np.pi * opening, np.pi * opening, n_rays)
        raycasts = raycast_entities(
            entity_poses=entitiy_poses,
            sector_limit_angles=sector_limit_angles,
            far_plane=far_plane,
        )
        # mostly the results are equal to at least 7 decimals,
        # but tests get flaky.
        np.testing.assert_almost_equal(*raycasts, decimal=6)


def test_compare_entities_raycast():
    random_diffs_entities(1000)


def test_compare_walls_raycast():
    random_diffs_walls(1000)
