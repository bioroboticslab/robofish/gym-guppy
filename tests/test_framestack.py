from robofish.gym_guppy.wrappers import FrameStack, IndexedFrameStack
from robofish.gym_guppy.envs import ConfigurableGuppyEnv
import numpy as np


def _rollout(fs: FrameStack, n_steps: int) -> np.ndarray:
    """Rollout the environment for `n_steps` steps and return the observations.

    Args:
        fs (FrameStack): The wrapped environment.
        n_steps (int): The number of steps to rollout.

    Returns:
        observations (np.ndarray): The observations.
    """
    observations = []
    obs, _ = fs.reset()
    observations.append(obs)
    for _ in range(n_steps):
        obs, *_ = fs.step(np.zeros((2,)))
        observations.append(obs)
    return np.stack(observations)


def test_framestack() -> None:
    """Test if the framestack wrapper works as expected."""
    k = 3
    env = ConfigurableGuppyEnv(guppy_type="DummyGuppy", robot_type="TurnSpeedRobot")
    fs = FrameStack(env=env, k=k)
    obs = _rollout(fs, n_steps=k * 2)

    assert obs.shape[1] == k
    assert obs.shape[1:] == fs.observation_space.shape

    # compare last step and previous step
    # they should have two frames in common
    np.testing.assert_almost_equal(obs[-1, -2], obs[-2, -1])
    np.testing.assert_almost_equal(obs[-1, -3], obs[-2, -2])

    # compare last step and two steps before
    # they should have one frame in common
    np.testing.assert_almost_equal(obs[-1, -3], obs[-3, -1])


def test_indexed_framestack() -> None:
    """Test if the indexed framestack wrapper works as expected."""
    env = ConfigurableGuppyEnv(guppy_type="DummyGuppy", robot_type="TurnSpeedRobot")
    indices = [0, 1, 4]
    fs = IndexedFrameStack(env=env, frame_indices=indices)
    obs = _rollout(fs, n_steps=(max(indices) + 1) * 2)

    assert obs.shape[1] == len(indices)
    assert obs.shape[1:] == fs.observation_space.shape
    for i, index in enumerate(fs.frame_indices):
        # last observation, stack index `i` should be equal to going back by
        # `index` steps and taking the last (most current) stack index
        np.testing.assert_almost_equal(obs[-1, i], obs[index, -1])
        # test the same for the previous observation
        np.testing.assert_almost_equal(obs[-2, i], obs[index, -2])
