"""
Test if TurnSpeedRobot and TurnSpeedGuppy behave in the same ways, especially
when robot action frequency and guppy action frequency are at non default
values.
"""
import numpy as np
from robofish.gym_guppy.envs import ConfigurableGuppyEnv, VariableStepGuppyEnv
from robofish.gym_guppy.guppies import TurnSpeedRobot, TurnSpeedGuppy
import pytest


SPEED = 0.1


class StepCountRobot(TurnSpeedRobot):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.step_count = 0
        self.action_count = 0
        self.time_s = 0

    def set_action(self, action):
        super().set_action(action)
        self.action_count += 1

    def step(self, time_step):
        super().step(time_step)
        self.step_count += 1
        self.time_s += time_step


class StepCountGuppy(TurnSpeedGuppy):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.step_count = 0
        self.action_count = 0
        self.time_s = 0

    def compute_next_action(self, state: np.ndarray, kd_tree=None):
        self.turn = 0.0
        self.speed = SPEED
        self.action_count += 1

    def step(self, time_step):
        super().step(time_step)
        self.step_count += 1
        self.time_s += time_step


@pytest.fixture(params=[0.01, 0.10, 0.5, 1.0, 2.0])
def env(request):
    return ConfigurableGuppyEnv(
        robot_type=StepCountRobot,
        guppy_type=StepCountGuppy,
        robot_pose_rng=lambda *args, **kwargs: np.zeros(3),
        guppy_pose_rng=lambda *args, **kwargs: np.zeros((1, 3)),
        time_step_s=request.param,
    )


@pytest.mark.parametrize("n_robot_actions", [1, 2, 3])
def test_count_steps(env, n_robot_actions):
    """Test if robot and guppy actions are called the correct number of times."""
    env.reset()
    guppy = next(env.guppies)
    robot = env.robot

    for _ in range(n_robot_actions):
        env.step(np.zeros((2,)))

    assert robot.action_count == n_robot_actions
    assert guppy.action_count == n_robot_actions
    assert robot.time_s == guppy.time_s


def test_distance(env):
    """
    Test moving with fixed velocity, using TurnSpeedRobot and TurnSpeedGuppy.
    Both should move with SPEED in the same direction.
    The time that passed depends on the robot action frequency.
    The distance moved depends on the time that passed and SPEED.
    Both should be at the same expected distance from center after one step.
    """
    env.reset()
    guppy = next(env.guppies)
    robot = env.robot

    action = np.float32([0, SPEED])
    env.step(action)

    measured_distance_robot = np.linalg.norm(robot.get_position())
    expected_distance_robot = SPEED * robot.time_s

    iid = np.linalg.norm(robot.get_position() - guppy.get_position())

    assert not guppy._friction
    assert not robot._friction
    assert np.allclose(measured_distance_robot, expected_distance_robot, atol=0.005)
    assert iid < 0.005
