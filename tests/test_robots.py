import numpy as np
from robofish.gym_guppy.envs import ConfigurableGuppyEnv
from robofish.gym_guppy.guppies import TurnSpeedRobot
import pytest


@pytest.mark.parametrize("forward", np.linspace(0.01, 0.49, 10))
@pytest.mark.parametrize("time_step_s", (0.04, 0.05))
def test_turn_speed_robot_turn_back(forward, time_step_s):
    env = ConfigurableGuppyEnv(
        robot_type=TurnSpeedRobot,
        num_guppies=0,
        robot_pose_rng=lambda *args, **kwargs: np.zeros(
            (3,)
        ),  # deterministic init pose
        time_step_s=time_step_s,
    )
    init_obs, _ = env.reset()

    # sanity check: not moving should not change anything
    obs, *_ = env.step([0, 0])
    assert (init_obs == obs).any()

    # move for one second
    n_steps = int(1 / time_step_s)
    print(n_steps)

    # move forward
    for _ in range(n_steps):
        obs, *_ = env.step([0, forward])
    assert np.allclose(obs, np.float32([[forward, 0, 0]]), atol=0.005)

    # turn and move back
    for i in range(n_steps):
        obs, *_ = env.step([np.pi if not i else 0, forward])

    # turn again to get back to initial pose
    obs, *_ = env.step([-np.pi, 0])
    assert np.allclose(obs, init_obs, atol=0.03)
