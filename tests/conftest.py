import pytest
from robofish.gym_guppy.envs import GuppyEnv
from robofish.gym_guppy.guppies import BoostCouzinGuppy
from robofish.gym_guppy.guppies import TurnBoostRobot


class Env(GuppyEnv):
    def _reset(self):
        self._add_guppy(
            BoostCouzinGuppy(
                world=self.world,
                world_bounds=self.world_bounds,
                position=[0.1, 0.0],
                orientation=0.0,
            )
        )
        self._add_robot(
            TurnBoostRobot(
                world=self.world,
                world_bounds=self.world_bounds,
                position=[0.0, 0.0],
                orientation=0.0,
            )
        )


@pytest.fixture()
def env():
    return Env()
