"""This module contains the reward functions related to robot and fish poses."""
import numpy as np
from numba import njit

from robofish.gym_guppy import GuppyEnv
from robofish.gym_guppy.tools.math import row_norm
from robofish.gym_guppy.tools.reward_function import (
    reward_function,
    reward_function_with_args,
)

__all__ = [
    "approach_swarm_reward",
    "follow_reward",
    "in_zoi_gate",
    "in_zoi_reward",
    "in_zor_gate",
    "near_walls_punishment",
    "neg_robot_swarm_dist",
    "negative_distance_to_swarm",
    "proximity_to_center_reward",
    "wall_avoidance_gate",
]


@reward_function
def negative_distance_to_swarm(env: GuppyEnv, _state, _action, next_state):
    """Reward is negative distance to swarm.

    Args:
        env: The environment.
        _state: The current state. (unused)
        _action: The action taken. (unused)
        next_state: The next state.

    Returns:
        The reward.
    """
    robot_id = env.robots_idx[0]
    robot_state = next_state[robot_id, :]
    swarm_state = np.array([s for i, s in enumerate(next_state) if i != robot_id])
    return [-1 * np.sum(row_norm(swarm_state - robot_state))]


@reward_function
def neg_robot_swarm_dist(env: GuppyEnv, _state, _action, next_state):
    """Reward is the negative distance between the robot and the swarm.

    Args:
        env: The environment.
        _state: The current state. (unused)
        _action: The action taken. (unused)
        next_state: The next state.

    Returns:
        The reward.
    """
    robot_id = env.robots_idx[0]
    robot_swarm_dist = _robot_swarm_dist(next_state, robot_id)
    return -np.mean(robot_swarm_dist)


@njit(fastmath=True)
def _extract_swarm_state(state, robot_id):
    """Extract the state of the swarm, excluding the robot.

    Args:
        state: The state of the environment.
        robot_id: The id of the robot.

    Returns:
        The state of the swarm.
    """
    if robot_id:
        return np.concatenate((state[:robot_id, :], state[robot_id + 1 :, :]))
    else:
        return state[robot_id + 1 :, :]


@njit(fastmath=True)
def _robot_swarm_dist(state, robot_id):
    """Compute the distance between the robot and the swarm.

    Args:
        state: The state of the environment.
        robot_id: The id of the robot.

    Returns:
        The distance between the robot and the swarm.
    """
    swarm_state = _extract_swarm_state(state, robot_id)
    robot_state = state[robot_id, :]
    return row_norm(swarm_state[:, :2] - robot_state[:2])


@reward_function
def follow_reward(env: GuppyEnv, state, _action, next_state):
    """Reward based on the follow metric.

    Args:
        env: The environment.
        state: The current state.
        _action: The action taken. (unused)
        next_state: The next state.

    Returns:
        The reward.
    """
    robot_id = env.robots_idx[0]
    env_agents_before = _extract_swarm_state(state, robot_id)[:, :2]
    env_agents_now = _extract_swarm_state(next_state, robot_id)[:, :2]
    fish_swim_vec = env_agents_now - env_agents_before
    rob_fish_vec = state[robot_id, :2] - env_agents_before
    inner = fish_swim_vec.dot(rob_fish_vec.T)
    norm_b = row_norm(rob_fish_vec)
    follow_metric = inner / norm_b
    follow_metric = np.where(np.isnan(follow_metric), 0.0, follow_metric)
    reward = np.mean(follow_metric)
    return reward


@reward_function
def approach_swarm_reward(env: GuppyEnv, state, _action, next_state):
    """Reward based on the approach metric.

    Args:
        env: The environment.
        state: The current state.
        _action: The action taken. (unused)
        next_state: The next state.

    Returns:
        The reward.
    """
    robot_id = env.robots_idx[0]
    norm_before = _robot_swarm_dist(state, robot_id)
    norm_after = _robot_swarm_dist(next_state, robot_id)
    difference = norm_before - norm_after
    return np.mean(difference)


@reward_function_with_args
def proximity_to_center_reward(
    env: GuppyEnv, _state, _action, next_state, half_diagonal
):
    """Reward is the proximity to the center of the environment.

    Args:
        env: The environment.
        _state: The current state.
        _action: The action taken.
        next_state: The next state.
        half_diagonal: Half the diagonal of the environment.

    Returns:
        The reward.
    """
    robot_id = env.robots_idx[0]
    env_agents_coordinates = np.array(
        [s[:2] for i, s in enumerate(next_state) if i != robot_id]
    )
    norm_to_center = row_norm(env_agents_coordinates)
    return (half_diagonal - np.mean(norm_to_center)) / half_diagonal


@reward_function_with_args
def near_walls_punishment(env: GuppyEnv, _state, _action, next_state, epsilon):
    """Reward is -0.1 if the robot is near the walls, otherwise 0.

    This reward function was successfully used many times and seems to work well
    to make the policy avoid walls.

    Args:
        env: The environment.
        _state: The current state. (unused)
        _action: The action taken. (unused)
        next_state: The next state.
        epsilon: The distance from the wall.

    Returns:
        The reward.
    """
    min_wall_dist = np.min(
        np.abs(np.asarray(env.world_bounds) - next_state[env.robots_idx[0], :2])
    )
    if min_wall_dist < epsilon:
        return -0.1
    return 0.0


@reward_function_with_args
def wall_avoidance_gate(env: GuppyEnv, _state, _action, next_state, epsilon):
    """Reward is 1 if the robot is far from the walls, otherwise 0.

    This reward function may be multiplied with another function, this is why it
    is called a "gate".

    Args:
        env: The environment.
        _state: The current state. (unused)
        _action: The action taken. (unused)
        next_state: The next state.
        epsilon: The distance from the wall.

    Returns:
        The reward.
    """
    min_wall_dist = np.min(
        np.abs(np.asarray(env.world_bounds) - next_state[env.robots_idx[0], :2])
    )
    if min_wall_dist > epsilon:
        return 1.0
    return 0.0


@reward_function
def in_zor_gate(env: GuppyEnv, _state, _action, next_state):
    """Reward is 0 if the robot is in the zone of repulsion, otherwise 1.

    This reward function may be multiplied with another function, this is why it
    is called a "gate".

    Args:
        env: The environment.
        _state: The current state. (unused)
        _action: The action taken. (unused)
        next_state: The next state.

    Returns:
        The reward.
    """
    zor = np.array([g.zor for g in env.guppies])
    robot_id = env.robots_idx[0]
    robot_swarm_dist = _robot_swarm_dist(next_state, robot_id)
    if np.any(robot_swarm_dist <= zor):
        return 0.0
    return 1.0


@reward_function
def in_zoi_gate(env: GuppyEnv, _state, _action, next_state):
    zoi = np.array([g.zoi for g in env.guppies])
    robot_id = env.robots_idx[0]
    robot_swarm_dist = _robot_swarm_dist(next_state, robot_id)
    return 1.0 + np.mean(zoi - np.maximum(robot_swarm_dist, zoi))


@reward_function
def in_zoi_reward(env: GuppyEnv, _state, _action, next_state):
    zoi = np.array([g.zoi for g in env.guppies])
    robot_id = env.robots_idx[0]
    robot_swarm_dist = _robot_swarm_dist(next_state, robot_id)
    return np.sum(robot_swarm_dist <= zoi)
