"""Reward wrappers."""
import numpy as np
from numba import njit

from robofish.gym_guppy.tools.reward_function import reward_wrapper


clipped_reward_doc = """Clip the reward to the given range.

Args:
    reward: The reward to clip.
    min_reward: The minimum reward.
    max_reward: The maximum reward.

Returns:
    The clipped reward.
"""


@reward_wrapper
@njit(fastmath=True)
def clipped_reward(  # noqa: D103 (https://github.com/numba/numba/issues/134)
    reward,
    min_reward=-np.inf,
    max_reward=np.inf,
):
    return min(max(reward, min_reward), max_reward)


exp_doc = """Exponentiate the reward.

Args:
    reward: The reward to exponentiate.

Returns:
    The exponentiated reward.
"""


@reward_wrapper
@njit(fastmath=True)
def exp(reward):  # noqa: D103 (https://github.com/numba/numba/issues/134)
    return np.exp(reward)
