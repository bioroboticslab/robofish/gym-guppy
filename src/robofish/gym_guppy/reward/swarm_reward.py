"""Reward function for the swarm."""
import numpy as np
from numba import njit

from robofish.gym_guppy.tools.math import row_norm
from robofish.gym_guppy.tools.reward_function import reward_function

__all__ = ["negative_distance_to_center"]


@reward_function
@njit(fastmath=True)
def negative_distance_to_center(_state, _action, next_state):
    """Reward is negative distance of the swarm to center.

    Args:
        _state: The current state. (unused)
        _action: The action taken. (unused)
        next_state: The next state.

    Returns:
        The reward.
    """
    # reward is high, if fish are in the center of the tank
    return -np.sum(row_norm(next_state))
