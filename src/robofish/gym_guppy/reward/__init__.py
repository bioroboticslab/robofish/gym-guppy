"""Reward functions."""
from .reward_wrapper import clipped_reward, exp, clipped_reward_doc, exp_doc
from .robot_reward import (
    negative_distance_to_swarm,
    follow_reward,
    approach_swarm_reward,
    proximity_to_center_reward,
    wall_avoidance_gate,
    in_zor_gate,
    in_zoi_gate,
    in_zoi_reward,
)
from .swarm_reward import negative_distance_to_center
from .goal_reward import goal_reward, relative_goal_reward

__all__ = [
    "clipped_reward",
    "exp",
    "negative_distance_to_swarm",
    "follow_reward",
    "approach_swarm_reward",
    "proximity_to_center_reward",
    "wall_avoidance_gate",
    "in_zor_gate",
    "in_zoi_gate",
    "in_zoi_reward",
    "negative_distance_to_center",
    "goal_reward",
    "relative_goal_reward",
]

__pdoc__ = {
    "in_zor_gate": False,
    "in_zoi_gate": False,
    "in_zoi_reward": False,
    "exp": exp_doc,  # https://github.com/numba/numba/issues/134
    "clipped_reward": clipped_reward_doc,  # https://github.com/numba/numba/issues/134
}
