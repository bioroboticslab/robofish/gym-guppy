# isort:skip_file
# skip sorting because of the "noqa" below
import abc
from itertools import cycle
from typing import Callable, List, Tuple, Type, Union

import numpy as np

from robofish.gym_guppy.envs import GuppyEnv

from robofish.gym_guppy.guppies import (
    AdaptiveCouzinGuppy,
    BiasedAdaptiveCouzinGuppy,
    Guppy,
    PolarCoordinateTargetRobot,
    Robot,
    VelocityControlledAgent,
)

# these imports are needed for eval to work when passing robots or guppies as strings
from robofish.gym_guppy.guppies import (  # noqa: F401
    BaseCouzinGuppy,
    BoostCouzinGuppy,
    BoostGuppy,
    ClassicCouzinGuppy,
    DummyGuppy,
    GoToRobot,
    PerturbedAdaptiveCouzinGuppy,
    RandomTurnBoostGuppy,
    TorqueGuppy,
    TurnBoostGuppy,
    TurnRobot,
    TurnSpeedGuppy,
    TurnSpeedRobot,
)


def robot_pose_sample_normal(bounds, rng: np.random.Generator):
    """Sample a robot pose from a normal distribution with mean at the center.

    Args:
        bounds (Tuple[np.ndarray]): The world bounds.
        rng (np.random.Generator): The random number generator.

    Returns:
        np.ndarray: The sampled pose.
    """
    position = rng.normal(scale=0.15, size=(2,))
    position = np.minimum(np.maximum(position, bounds[0]), bounds[1])

    orientation = rng.random() * 2 * np.pi - np.pi

    return np.r_[position, orientation]


def robot_pose_sample_uniform(bounds, rng: np.random.Generator):
    """Sample a robot pose from a uniform distribution.

    Args:
        bounds (Tuple[np.ndarray]): The world bounds.
        rng (np.random.Generator): The random number generator.

    Returns:
        np.ndarray: The sampled pose.
    """
    size = bounds[1] - bounds[0]
    position = (rng.random(2) * size + bounds[0]) * 0.9

    orientation = rng.random() * 2 * np.pi - np.pi

    return np.r_[position, orientation]


def swarm_pose_sample_normal(num_guppies: int, bounds, rng: np.random.Generator):
    """Sample guppy poses from a normal distribution with mean at the center.

    Args:
        num_guppies (int): The number of guppies.
        bounds (Tuple[np.ndarray]): The world bounds.
        rng (np.random.Generator): The random number generator.

    Returns:
        np.ndarray: The sampled poses.
    """
    size = bounds[1] - bounds[0]
    swarm_position = (rng.random(2) * size + bounds[0]) * 0.95

    fish_positions = rng.normal(loc=swarm_position, scale=0.05, size=(num_guppies, 2))
    fish_positions = np.minimum(np.maximum(fish_positions, bounds[0]), bounds[1])

    fish_orientations = rng.random((num_guppies, 1)) * 2 * np.pi - np.pi

    return np.c_[fish_positions, fish_orientations]


def let_guppies_set_init_pose(num_guppies: int, bounds, rng: np.random.Generator):
    """Let guppies set their own initial poses.

    Dummy function used in cases when guppies need to set their own initial poses.

    Args:
        num_guppies (int): The number of guppies.
        bounds (Tuple[np.ndarray]): The world bounds.
        rng (np.random.Generator): The random number generator.
    """
    return np.full((num_guppies, 3), np.nan)


class ConfigurableGuppyEnv(GuppyEnv, abc.ABC):
    """Configurable environment that inherits from GuppyEnv."""

    def __init__(
        self,
        robot_type: Union[str, Type[Robot]] = PolarCoordinateTargetRobot,
        robot_args: dict = None,
        robot_pose_rng: Union[
            str, Callable[[Tuple[np.ndarray]], np.ndarray]
        ] = robot_pose_sample_normal,
        guppy_type: Union[
            str, Type[Guppy], List[Union[str, Type[Guppy]]]
        ] = AdaptiveCouzinGuppy,
        guppy_args: Union[dict, List[dict]] = None,
        guppy_pose_rng: Union[
            str, Callable[[int, Tuple[np.ndarray]], np.ndarray]
        ] = swarm_pose_sample_normal,
        num_guppies: int = 1,
        num_robots: int = 1,
        controller_params=None,
        **kwargs,
    ):
        """Initialize the environment.

        Args:
            robot_type: The robot type. Defaults to PolarCoordinateTargetRobot.
            robot_args: The robot arguments. Defaults to None.
            robot_pose_rng: The robot pose sampling function. Defaults to robot_pose_sample_normal.
            guppy_type: The guppy type. Defaults to AdaptiveCouzinGuppy.
            guppy_args: The guppy arguments. Defaults to None.
            guppy_pose_rng: The guppy pose sampling function. Defaults to swarm_pose_sample_normal.
            num_guppies: The number of guppies. Defaults to 1.
            num_robots: The number of robots. Defaults to 1.
            controller_params: The PID controller parameters for the robot. Defaults to None.
            **kwargs: Additional arguments.

        Raises:
            ValueError:
                If the number of guppy types does not match the number of guppy arguments.
        """
        if isinstance(robot_type, str):
            robot_type = eval(robot_type)
        self.robot_type = robot_type
        self.robot_args = robot_args or {}

        if controller_params:
            self.robot_args["ctrl_params"] = controller_params

        if (
            "ctrl_params" not in self.robot_args
            and self.robot_type
            and num_robots
            and issubclass(self.robot_type, VelocityControlledAgent)
        ):
            raise ValueError(
                "Controller parameters must be specified for "
                "robots that use PID controllers."
            )

        if not isinstance(guppy_type, list):
            guppy_type = [guppy_type]
        for i, gt in enumerate(guppy_type):
            if isinstance(gt, str):
                guppy_type[i] = eval(gt)
        self.guppy_type = guppy_type
        self.guppy_args = guppy_args or {}
        if not isinstance(self.guppy_args, list):
            self.guppy_args = [self.guppy_args]
        self._num_guppies = num_guppies
        self._num_robots = num_robots

        if isinstance(robot_pose_rng, str):
            robot_pose_rng = eval(robot_pose_rng)
        self._robot_pose_rng = robot_pose_rng
        if isinstance(guppy_pose_rng, str):
            guppy_pose_rng = eval(guppy_pose_rng)
        self._swarm_pose_rng = guppy_pose_rng

        super(ConfigurableGuppyEnv, self).__init__(**kwargs)

    def _reset(self):
        super(ConfigurableGuppyEnv, self)._reset()

        rng = (
            self._np_random
            if hasattr(self, "_np_random") and self._np_random is not None
            else np.random.default_rng()
        )

        if self.robot_type and self._num_robots:
            robot_poses = []
            for i in range(self._num_robots):
                pose = self._robot_pose_rng(self.world_bounds, rng=rng)
                robot_poses.append(pose)
                self._add_robot(
                    self.robot_type(
                        world=self.world,
                        world_bounds=self.world_bounds,
                        position=pose[:2],
                        orientation=pose[2],
                        **self.robot_args,
                    )
                )
            robot_poses = np.stack(robot_poses)
        else:
            robot_poses = None

        fish_poses = self._swarm_pose_rng(self._num_guppies, self.world_bounds, rng=rng)
        guppy_args_cycle = cycle(self.guppy_args)
        guppy_type_cycle = cycle(self.guppy_type)
        for i, pose in enumerate(fish_poses):
            guppy_args = dict(
                world=self.world,
                world_bounds=self.world_bounds,
                position=pose[:2],
                orientation=pose[2],
                robot_poses=robot_poses,
                id=i + len(list(self.robots)),
            )
            guppy_args.update(next(guppy_args_cycle))
            guppy_type = next(guppy_type_cycle)
            if issubclass(guppy_type, AdaptiveCouzinGuppy):
                guppy_args["unknown_agents"] = (
                    list(self.robots) if self.robot_type else []
                )
            if issubclass(guppy_type, BiasedAdaptiveCouzinGuppy):
                guppy_args["repulsion_points"] = [[0.0, 0.0]]
            self._add_guppy(guppy_type(**guppy_args))
