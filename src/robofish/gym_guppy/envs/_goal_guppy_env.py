import abc
from typing import List

import numpy as np

from ._guppy_env import GuppyEnv


class GoalGuppyEnv(GuppyEnv, abc.ABC):
    """Abstract base class for environments that use goals."""

    def __init__(
        self, *, change_goal_threshold=0.05, done_when_goal_reached=True, **kwargs
    ):
        """Initialize the environment.

        change_goal_threshold:
            The distance threshold for determining whether the agenthas reached the goal
        done_when_goal_reached:
            A flag indicating whether the environment should be marked as done when the
            goal is reached
        kwargs:
            Additional arguments passed to the parent class
        """
        self.change_goal_threshold = change_goal_threshold
        super(GoalGuppyEnv, self).__init__(**kwargs)
        self._done_when_goal_reached = done_when_goal_reached

    @property
    @abc.abstractmethod
    def desired_goal(self):
        """Get the current desired goal for the agent.

        This should be overridden by subclasses.
        """
        raise NotImplementedError

    @desired_goal.setter
    @abc.abstractmethod
    def desired_goal(self, new_goal):
        """Set the desired goal for the agent.

        This should be overridden by subclasses.
        """
        raise NotImplementedError

    @property
    def achieved_goal(self) -> np.ndarray:
        """Get the current achieved goal for the agent.

        The achieved goal is compared with the desired goal to determine if the
        goal is reached.
        If there is more than one guppy, the achieved goal is the average
        position of the guppies. If there are no guppies the robot's position is
        used.
        """
        if self.num_guppies:
            return np.mean([g.get_position() for g in self.guppies], axis=0)
        else:
            return self.robot.get_position()

    def goal_reached(self) -> bool:
        """Determine whether the agent has reached the desired goal.

        Depends on the current desired goal and achieved goal and the
        change_goal_threshold.

        Returns:
            bool : indicating whether the goal has been reached or not.
        """
        return (
            np.linalg.norm(self.desired_goal - self.achieved_goal)
            <= self.change_goal_threshold
        )

    @abc.abstractmethod
    def _update_goal(self):
        """Update the desired goal for the agent.

        This should be overridden by subclasses.
        """
        raise NotImplementedError

    def get_done(self, state, action):
        """Determine whether the environment is done.

        Args:
            state : current state of the environment
            action : last action taken by the agent

        Returns:
            bool : indicating whether the environment is done or not.
        """
        return self._done_when_goal_reached and self.goal_reached()

    def step(self, action: np.ndarray):
        """Run one timestep of the environment's dynamics.

        Args:
            action (np.ndarray): an action provided by the agent

        Returns:
            observation (object): agent's observation of the current environment
            reward (float) : amount of reward returned after previous action
            done (bool): whether the episode has ended
            info (dict): contains entity poses etc.
        """
        ret_val = super().step(action)
        if self.goal_reached():
            self._update_goal()
            self._info["goal_reached"][-1] = True
        return ret_val

    def _update_log(self, action: np.ndarray) -> None:
        """Update the internal log of the environment that.

        The log is placed in the info dict returned by the step method.

        Args:
            action: action taken by the agent
        """
        super()._update_log(action=action)
        self._info["goal_reached"].append(False)
        self._info["goals"].append(self.desired_goal)

    def _get_info_keys(self) -> List[str]:
        """Get the keys of the info dict.

        The info dict is returned by the step method.

        Returns:
            List[str]: List of keys
        """
        return super()._get_info_keys() + [
            "goal_reached",
            "goals",
        ]
