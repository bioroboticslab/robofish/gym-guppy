"""
Environment classes for reinforcement learning and other uses.

For simple use cases, the `ConfigurableGuppyEnv` can be used directly.
For reinforcement learning, there are more specific environments in
`robofish.rl` that inherit from the environments of this package.
"""
# isort:skip_file
from ._guppy_env import GuppyEnv
from ._configurable_guppy_env import ConfigurableGuppyEnv
from ._goal_guppy_env import GoalGuppyEnv
from ._variable_step_guppy_env import VariableStepGuppyEnv, VariableStepGoalGuppyEnv
from ._mixins import RenderCouzinZonesMixin, RenderGoalMixin

__all__ = [
    "GuppyEnv",
    "ConfigurableGuppyEnv",
    "GoalGuppyEnv",
    "VariableStepGuppyEnv",
    "VariableStepGoalGuppyEnv",
    "RenderCouzinZonesMixin",
    "RenderGoalMixin",
]
__pdoc__ = {
    "RenderCouzinZonesMixin": False,
    "RenderGoalMixin": False,
    "VariableStepGuppyEnv": False,
    "VariableStepGoalGuppyEnv": False,
    "ConfigurableGuppyEnv.render_mode": False,
    "GoalGuppyEnv.render_mode": False,
    "GuppyEnv.render_mode": False,
    "GuppyEnv.render": False,
    "GuppyEnv.screen_size": False,
    "GuppyEnv.screen_width": False,
    "GuppyEnv.screen_height": False,
    "GuppyEnv.destroy": False,
    "GuppyEnv.close": False,
}
