import abc
import types
import warnings
from typing import Any, Dict, Iterator, List, Union

from deprecated import deprecated
import gymnasium as gym
from gymnasium.utils.step_api_compatibility import convert_to_terminated_truncated_step_api
import numpy as np
from Box2D import b2PolygonShape, b2Vec2, b2World
from robofish.gym_guppy import Robot
from robofish.gym_guppy.bodies import Body, _world_scale
from robofish.gym_guppy.guppies import (
    Agent,
    Guppy,
    VelocityControlledAgent,
)
from robofish.gym_guppy.tools.reward_function import (
    RewardConst,
    RewardFunctionBase,
    reward_registry,
)
from scipy.spatial import cKDTree


try:

    class ABCDecorator(abc.ABCMeta, gym.core._EnvDecorator):
        """Avoid a metaclass conflict.

        This became necessary since gym.Env uses a metaclass as well.
        """

        pass

except AttributeError:
    ABCDecorator = abc.ABCMeta


class GuppyEnv(gym.Env, metaclass=ABCDecorator):
    """Abstract base class of all environments in the guppy gym.

    Implements the OpenAI gym interface.
    """

    metadata = {
        "render.modes": ["human", "video", "rgb_array"],
        "video.frames_per_second": None,
    }

    world_size = world_width, world_height = 1.0, 1.0
    screen_size = screen_width, screen_height = 800, 800
    state_history_stepsize = None

    __sim_steps_per_second = 100
    __sim_velocity_iterations = 8
    __sim_position_iterations = 3

    def __init_subclass__(cls, **kwargs):
        """This is called when a subclass is created."""
        # TODO remove this
        pass

    def __new__(cls, *args, **kwargs):
        """Set class variables."""
        cls.sim_steps_per_second = cls.__sim_steps_per_second
        cls.step_time = 1.0 / cls.__sim_steps_per_second
        cls.world_x_range = -cls.world_width / 2, cls.world_width / 2
        cls.world_y_range = -cls.world_height / 2, cls.world_height / 2
        cls.world_bounds = (
            np.float32([-cls.world_width / 2, -cls.world_height / 2]),
            np.float32([cls.world_width / 2, cls.world_height / 2]),
        )

        cls.__fps = 25

        return super(GuppyEnv, cls).__new__(cls)

    def __init__(
        self,
        max_steps=None,
        time_step_s=None,
        robot_action_frequency_hz=None,
        guppy_action_frequency_hz=None,
        *args,
        **kwargs,
    ):
        """Initialize the environment.

        Args:
            max_steps: The maximum number of steps the environment is allowed to run.
            time_step_s: The time step of the environment in seconds.
            robot_action_frequency_hz: The frequency of robot actions in Hz.
            guppy_action_frequency_hz: The frequency of guppy actions in Hz.
        """
        assert not args, args
        assert not kwargs, kwargs
        super(GuppyEnv, self).__init__(*args, **kwargs)

        self._setup_frequencies(
            time_step_s=time_step_s,
            robot_action_frequency_hz=robot_action_frequency_hz,
            guppy_action_frequency_hz=guppy_action_frequency_hz,
        )

        self.metadata["video.frames_per_second"] = (
            self.__sim_steps_per_second / self.__steps_per_action
        )
        self._sim_steps = 0
        self._action_steps = 0
        self.__reset_counter = 0
        self._max_steps = max_steps

        # create the world in Box2D
        self.world = b2World(gravity=(0, 0), doSleep=True)
        self.tank = self.world.CreateStaticBody(position=(0.0, 0.0))

        wall_shape = b2PolygonShape()
        wall_width = 0.05
        wall_eps = 0.005

        for s in -1, 1:
            wall_shape.SetAsBox(
                wall_width / 2 * _world_scale,
                (self.world_height / 2 + wall_width) * _world_scale,
                b2Vec2(
                    (self.world_width / 2 + wall_width / 2 - wall_eps)
                    * _world_scale
                    * s,
                    0.0,
                ),
                0.0,
            )
            self.tank.CreateFixture(shape=wall_shape)
            wall_shape.SetAsBox(
                (self.world_width / 2 + wall_width) * _world_scale,
                wall_width / 2 * _world_scale,
                b2Vec2(
                    0.0,
                    (self.world_height / 2 + wall_width / 2 - wall_eps)
                    * _world_scale
                    * s,
                ),
                0.0,
            )
            self.tank.CreateFixture(shape=wall_shape)

        self.kd_tree = None

        self.__agents: List[Agent] = []
        self.__robots_idx: List[int] = []
        self.__guppies_idx: List[int] = []

        self.__objects: List[Body] = []

        self._screen = None
        self.render_mode = "human"
        self.video_path = None

        self.set_reward(RewardConst(0.0))

        self.action_space = None
        self.observation_space = None
        self.state_space = None

        self._reset()
        self._info = self._get_init_info()

        self.action_space = self._get_action_space()
        self.observation_space = self._get_observation_space()
        self.state_space = self._get_state_space()

        self._step_world()

    def _setup_frequencies(
        self, time_step_s, robot_action_frequency_hz, guppy_action_frequency_hz
    ):
        """Set the frequency of robot and guppy actions.

        Args:
            time_step_s: The time step of the environment in seconds.
            robot_action_frequency_hz: The frequency of robot actions in Hz.
            guppy_action_frequency_hz: The frequency of guppy actions in Hz.
        """
        if time_step_s and (robot_action_frequency_hz or guppy_action_frequency_hz):
            # TODO change error msg after deprecation of time_step_s
            raise ValueError(
                "Please use either `robot_action_frequency_hz` and "
                "`guppy_action_frequency_hz` or `time_step_s`"
            )

        if (
            not time_step_s
            and not robot_action_frequency_hz
            and not guppy_action_frequency_hz
        ):
            # keep old default behavior
            # TODO deprecation warning
            self.__steps_per_action = 50
            self._guppy_steps_per_action = 10
        elif time_step_s:
            # TODO deprecation warning
            time_step = time_step_s * 100
            if not time_step.is_integer():
                raise ValueError(
                    "Due to limited simulation resolution, "
                    "time_step_s needs to be a multiple of 0.01."
                )
            time_step = int(time_step)
            self.__steps_per_action = time_step
            self._guppy_steps_per_action = time_step
        else:
            if robot_action_frequency_hz:
                self.__steps_per_action = int(
                    round(100 / adjust_frequency(robot_action_frequency_hz))
                )
            else:
                raise ValueError("No robot_action_frequency_hz provided.")

            assert hasattr(self, "_num_guppies")
            if guppy_action_frequency_hz:
                self._guppy_steps_per_action = int(
                    round(100 / adjust_frequency(guppy_action_frequency_hz))
                )
            elif self._num_guppies and not guppy_action_frequency_hz:
                raise ValueError("No guppy_action_frequency_hz provided.")
            else:
                # for convenience if there are no guppies,
                # guppy_action_frequency_hz does not need to be set
                self._guppy_steps_per_action = self.__steps_per_action

    @property
    def sim_steps(self):
        """Get the number of simulation steps that have been executed."""
        return self._sim_steps

    @property
    def _internal_sim_loop_condition(self):
        """Check if the internal simulation loop should continue."""
        return self._action_steps < self.__steps_per_action

    @property
    def _max_steps_reached(self):
        """Check if the maximum number of steps has been reached."""
        if self._max_steps is None:
            return False
        return self._sim_steps >= self._max_steps

    @property
    def robots(self) -> Iterator[Robot]:
        """Get an iterator over all robots in the environment."""
        for r_idx in self.__robots_idx:
            yield self.__agents[r_idx]

    @property
    def robot(self):
        """Get the first robot in the environment."""
        if len(self.__robots_idx):
            return self.__agents[self.__robots_idx[0]]
        else:
            return None

    @property
    def robots_idx(self):
        """Get the indices of all robots in the environment."""
        return tuple(self.__robots_idx)

    @property
    def num_robots(self):
        """Get the number of robots in the environment."""
        return len(self.__robots_idx)

    @property
    def guppies(self) -> Iterator[Guppy]:
        """Get an iterator over all guppies in the environment."""
        for g_idx in self.__guppies_idx:
            yield self.__agents[g_idx]

    @property
    def guppy_idx(self):
        """Get the indices of all guppies in the environment."""
        return tuple(self.__guppies_idx)

    @property
    def num_guppies(self):
        """Get the number of guppies in the environment."""
        return len(self.__guppies_idx)

    @property
    def objects(self):
        """Get all objects in the environment as a tuple."""
        return tuple(self.__objects)

    def _get_action_space(self):
        """Get the action space of the environment.

        This is a Box space with the same shape as the action space of the robots.

        Returns:
            The action space of the environment.
        """
        actions_low = np.asarray([r.action_space.low for r in self.robots])
        actions_high = np.asarray([r.action_space.high for r in self.robots])

        return gym.spaces.Box(
            low=actions_low.astype(np.float32),
            high=actions_high.astype(np.float32),
            dtype=np.float32,
        )

    def _get_observation_space(self):
        """Get the observation space of the environment."""
        return self._get_state_space()

    def _get_state_space(self):
        """Get the state space of the environment."""
        state_low = np.concatenate([self.world_bounds[0], [-np.inf]])
        state_low = np.tile(state_low, (self.num_robots + self.num_guppies, 1))
        state_high = np.concatenate([self.world_bounds[1], [np.inf]]).astype(np.float32)
        state_high = np.tile(state_high, (self.num_robots + self.num_guppies, 1))

        return gym.spaces.Box(
            low=state_low.astype(np.float32),
            high=state_high.astype(np.float32),
            dtype=np.float32,
        )

    @property
    def _steps_per_action(self):
        """Get the number of simulation steps per robot action."""
        return self.__steps_per_action

    @_steps_per_action.setter
    def _steps_per_action(self, spa: int):
        """Set the number of simulation steps per robot action."""
        warnings.warn(f"Setting steps_per_action to {spa}")
        self.__steps_per_action = spa

    def __add_agent(self, agent: Agent, left=False) -> bool:
        """Add an agent to the simulation.

        Args:
            agent: The agent to add.
            left: If True, the agent is added to the left of the list of agents.
                  Otherwise, it is added to the right.

        Returns:
            True if the agent was added successfully, False otherwise.
        """
        if agent in self.__agents:
            warnings.warn("Agent " + agent.id + " has already been registered before.")
            return False

        next_id = len(self.__agents)
        agent.id = next_id
        if left:
            self.__agents.insert(0, agent)
        else:
            self.__agents.append(agent)

        return True

    def _add_guppy(self, guppy: Guppy) -> bool:
        """Add a guppy to the simulation.

        Args:
            guppy: The guppy to add.

        Returns:
            True if the guppy was added successfully, False otherwise.
        """
        if self.__add_agent(guppy):
            self.__guppies_idx.append(guppy.id)
            return True
        return False

    def _add_robot(self, robot: Robot) -> bool:
        """Add a robot to the simulation.

        Args:
            robot: The robot to add.

        Returns:
            True if the robot was added successfully, False otherwise.
        """
        if self.__add_agent(robot):
            self.__robots_idx.append(robot.id)
            return True
        return False

    def _add_object(self, body: Body):
        """Add an object/body to the simulation.

        Args:
            body: The body to add.
        """
        self.__objects.append(body)

    def get_state(self) -> np.ndarray:
        """Get the state of all agents in the environment.

        Returns:
            The state of all agents in the environment.
        """
        return np.float32([a.get_state() for a in self.__agents])

    def get_robots_state(self):
        """Get the state of all robots in the environment."""
        return self.get_indexed_state(self.__robots_idx)

    def get_guppies_state(self):
        """Get the state of all guppies in the environment."""
        return self.get_indexed_state(self.__guppies_idx)

    def get_indexed_state(self, index):
        """Get the state of the agents with the given indices.

        Args:
            index: The indices of the agents to get the state of.

        Returns:
            The state of the agents with the given indices.
        """
        return np.float32([self.__agents[i].get_state() for i in index])

    def get_observation(self, state):
        """Get the observation of the environment.

        This method may be "overridden" by an observation wrapper.

        Args:
            state: The state of the environment.

        Returns:
            The observation.
        """
        return state

    def _reward_function(self, state, action, next_state):
        """Get the reward for the given state, action, and next state.

        Args:
            state: The state of the environment.
            action: The action taken by the robot.
            next_state: The next state of the environment.

        Returns:
            The reward for the given state, action, and next state.
        """
        return 0.0

    def set_reward(self, reward_function: Union[RewardFunctionBase, str]):
        """Set the reward function of the environment."""
        if isinstance(reward_function, RewardFunctionBase):
            self._reward_function = types.MethodType(reward_function, self)
        else:
            self._reward_function = types.MethodType(
                eval(reward_function, reward_registry), self
            )

    def get_reward(self, state, action, new_state):
        """Get the reward for the given state, action, and next state.

        Args:
            state: The state of the environment.
            action: The action taken by the robot.
            new_state: The next state of the environment.

        Returns:
            The reward for the given state, action, and next state.
        """
        return self._reward_function(state, action, new_state)

    def get_done(self, state, action):
        """Get whether the episode is done.

        Args:
            state: The state of the environment.
            action: The action taken by the robot.

        Returns:
            True if the episode is done, False otherwise.
        """
        return False

    def _get_init_info(self) -> Dict[str, List[Any]]:
        """Get the initialized info dict of the environment."""
        info = {key: [] for key in self._get_info_keys()}
        if self.num_guppies:
            info["guppy_poses"] = {id_: [] for id_ in self.guppy_idx}
        if self.num_robots:
            info["robot_poses"] = {id_: [] for id_ in self.robots_idx}
            info["init_robot_pose"] = {
                id_: r.get_pose() for id_, r in zip(self.robots_idx, self.robots)
            }
            if "targets" in info:
                info["targets"] = {
                    id_: []
                    for id_, r in zip(self.robots_idx, self.robots)
                    if isinstance(r, VelocityControlledAgent)
                }
        if self.num_guppies:
            info["init_guppy_pose"] = {
                id_: g.get_pose() for id_, g in zip(self.guppy_idx, self.guppies)
            }
        info["init_state"] = self.get_state()
        return info

    def get_info(self, state: np.ndarray, action: np.ndarray) -> Dict[str, List[Any]]:
        """Get the info dict of the environment."""
        return self._info

    def destroy(self):
        """Destroy the environment.

        Note: This method is called automatically when the environment is reset.
        Do not call it manually.
        """
        del self.__objects[:]
        del self.__agents[:]
        self.__guppies_idx.clear()
        self.__robots_idx.clear()
        if self._screen is not None:
            del self._screen
            self._screen = None

    def close(self):
        """Close the environment.

        Note: This method is called automatically when the environment is reset.
        Do not call it manually.
        """
        self.destroy()

    @abc.abstractmethod
    def _reset(self):
        """Reset the environment.

        Note: This method is called automatically when the environment is reset.
        Do not call it manually.
        """
        pass

    def reset(self, **kwargs):
        """Reset the environment.

        Args:
            return_info: Whether to return the info dict.
            **kwargs: Additional keyword arguments.

        Returns:
            The observation of the environment and the info dict if return_info is True.
        """
        super().reset(**kwargs)
        self.__reset_counter += 1
        self.destroy()
        self._sim_steps = 0
        self._reset()
        self._info = self._get_init_info()

        # step to resolve
        self._step_world()

        obs = self.get_observation(self.get_state())
        return obs, self._info

    def _update_log(self, action: np.ndarray) -> None:
        """Update the info dict of the environment.

        Args:
            action: The action taken by the robot.
        """
        self._info["time_s"].append(self.time)
        self._info["states"].append(self.get_state())
        if self.num_robots:
            self._info["actions"].append(action)
            for id_, r in zip(self.robots_idx, self.robots):
                self._info["robot_poses"].setdefault(id_, [])
                self._info["robot_poses"][id_].append(r.get_pose())
                if isinstance(r, VelocityControlledAgent):
                    self._info["targets"].setdefault(id_, [])
                    self._info["targets"][id_].append(r.target)
        if self.num_guppies:
            for id_, g in zip(self.guppy_idx, self.guppies):
                self._info["guppy_poses"].setdefault(id_, [])
                self._info["guppy_poses"][id_].append(g.get_pose())

    def _get_info_keys(self) -> List[str]:
        """Get the keys of the info dict of the environment."""
        keys = [
            "time_s",
            "states",
        ]
        if self.num_robots:
            keys += [
                "actions",
                "robot_poses",
            ]
            if any(isinstance(r, VelocityControlledAgent) for r in self.robots):
                keys.append("targets")
        if self.num_guppies:
            keys.append("guppy_poses")
        return keys

    def step(self, action: np.ndarray):
        """Step the environment with the given action.

        The robot action gets executed for a number of simulation steps that
        depends on the robot action frequency.
        Each guppy's `compute_next_action` method is called, possibly multiple times,
        depending on the `robot_action_frequency_hz` and the
        `guppy_action_frequency_hz`.

        Args:
            action: The action for the robots with shape (num_robots, dim_action_space).

        Returns:
            The observation of the environment, the reward, whether the episode is done
            and the info dict.
        """
        # state before action is applied
        state = self.get_state()

        action = np.atleast_2d(action)
        # apply action to robots
        for r, a in zip(self.robots, action):
            # assert r.action_space.contains(a)
            r.set_env_state(state, self.kd_tree)
            r.set_action(a)

        self._action_steps = 0

        if self.state_history_stepsize:
            self.state_history = []
        while self._internal_sim_loop_condition and not self._max_steps_reached:
            s = self.get_state()

            self._compute_guppy_actions(s)

            for a in self.__agents:
                a.step(self.step_time)

            # step world
            self.world.Step(
                self.step_time,
                self.__sim_velocity_iterations,
                self.__sim_position_iterations,
            )
            # self.world.ClearForces()

            self._update_log(action=action.flatten())

            self._sim_steps += 1
            self._action_steps += 1

            if (
                self.state_history_stepsize
                and self._sim_steps % self.state_history_stepsize == 0
            ):
                self.state_history.append(self.get_state())
            if self._screen is not None and self._sim_steps % 4 == 0:
                self.render()

        # state
        next_state = self.get_state()
        # assert self.state_space.contains(next_state)

        # update KD-tree
        self._update_kdtree(next_state)

        # observation
        observation = self.get_observation(next_state)

        # reward
        reward = self.get_reward(state, action, next_state)

        # done
        done = self.get_done(next_state, action) or self._max_steps_reached

        # info
        info = self.get_info(next_state, action)

        return convert_to_terminated_truncated_step_api(
            step_returns=(observation, reward, done, info)
        )

    def _update_kdtree(self, state):
        """Update the KD-tree with the given state.

        Args:
            state: The state of the environment.
        """
        self.kd_tree = cKDTree(state[:, :2])

    def _compute_guppy_actions(self, state):
        """Compute the next action for each guppy.

        Args:
            state: The state of the environment.
        """
        if self._sim_steps % self._guppy_steps_per_action == 0:
            self._update_kdtree(state)
            for i, g in enumerate(self.guppies):
                g.compute_next_action(state=state, kd_tree=self.kd_tree)

    def _step_world(self):
        """Step the world (physics simulation) for one simulation step.

        This method is called by the `step` method.
        """
        self.world.Step(
            self.step_time,
            self.__sim_velocity_iterations,
            self.__sim_position_iterations,
        )
        self.world.ClearForces()

        state = self.get_state()
        self._update_kdtree(state)

    @property
    def time(self):
        """Get the time since reset of the environment in seconds."""
        return self.sim_steps * self.step_time

    @deprecated(version="0.4.0", reason="Use `robofish-io-render` instead.")
    def render(self, mode=None):
        """Render the environment.

        Args:
            mode: The mode to render the environment in. Can be "human", "video" or
                "rgb_array". If None, the mode is set to the value of the `render_mode`
                attribute.
        """
        if mode:
            self.render_mode = mode
        else:
            mode = self.render_mode
        if mode == "human":
            return self._render_human()
        elif mode == "video":
            return self._render_human(display=False)
        elif mode == "rgb_array":
            return self._render_rgb_array()

    @deprecated(version="0.4.0", reason="Use `robofish-io-render` instead.")
    def _render_rgb_array(self):
        """Render the environment as an RGB array."""
        rgb_array = np.ones(self.screen_size + (3,), dtype=np.uint8) * 255

        scale = np.divide(self.screen_size, self.world_size) * (0.0, -1.0)
        offset = self.world_bounds[0]

        for r in self.robots:
            r_state = r.get_state()
            col, row = np.int32(np.round((r_state[:2] - offset) * scale))
            for r in range(max(row - 3, 0), min(row + 3, self.screen_height)):
                for c in range(max(col - 3, 0), min(col + 3, self.screen_width)):
                    rgb_array[r, c, 1:3] = 0

        for g in self.guppies:
            g_state = g.get_state()
            col, row = np.int32(np.round((g_state[:2] - offset) * scale))
            for r in range(max(row - 3, 0), min(row + 3, self.screen_height)):
                for c in range(max(col - 3, 0), min(col + 3, self.screen_width)):
                    rgb_array[r, c, 0:2] = 0

        return rgb_array

    @deprecated(version="0.4.0", reason="Use `robofish-io-render` instead.")
    def _render_human(self, display=True):
        """Render the environment in a human-friendly way.

        Args:
            display: Whether to display the rendered environment.
        """
        fps = self.__fps
        from robofish.gym_guppy.tools import rendering

        if self._screen is None:
            caption = self.spec.id if self.spec else ""
            if self.video_path:
                import os

                os.makedirs(self.video_path, exist_ok=True)
                _video_path = os.path.join(
                    self.video_path, str(self.__reset_counter) + ".mp4"
                )
            else:
                _video_path = None

            self._screen = rendering.GuppiesViewer(
                self.screen_width,
                self.screen_height,
                caption=caption,
                fps=fps,
                display=display,
                record_to=_video_path,
            )
            world_min, world_max = self.world_bounds
            self._screen.set_bounds(
                world_min[0], world_max[0], world_min[1], world_max[1]
            )
        elif self._screen.close_requested():
            self._screen.close()
            self._screen = None
            # TODO how to handle this event?

        # render table
        x_min, x_max = self.world_x_range
        y_min, y_max = self.world_y_range
        self._screen.draw_polygon(
            [(x_min, y_max), (x_min, y_min), (x_max, y_min), (x_max, y_max)],
            color=(255, 255, 255),
        )

        # allow to draw on table
        self._draw_on_table(self._screen)

        # render objects
        for o in self.__objects:
            o.draw(self._screen)

        # render guppies
        for a in self.__agents:
            a.draw(self._screen)

        # allow to draw on top
        self._draw_on_top(self._screen)

        self._screen.render()

    @deprecated(version="0.4.0", reason="Use `robofish-io-render` instead.")
    def _draw_on_table(self, screen):
        """Draw on the table.

        Note: This method is called by the `render` method, but it actually does
        not do anything.

        Args:
            screen: The screen to draw on.
        """
        pass

    @deprecated(version="0.4.0", reason="Use `robofish-io-render` instead.")
    def _draw_on_top(self, screen):
        """Draw on the top.

        Note: This method is called by the `render` method, but it actually does
        not do anything.

        Args:
            screen: The screen to draw on.
        """
        pass


class UnknownObjectException(Exception):
    """Exception that is raised when an unknown object is encountered."""

    # TODO remove this exception
    pass


class InvalidArgumentException(Exception):
    """Exception that is raised when an invalid argument is passed."""

    # TODO remove this exception
    pass


def adjust_frequency(f):
    """Adjust the frequency to the closest possible frequency.

    When using e.g. 100 Hz as the simulation step frequency, not every
    action frequency is possible, because the number of simulation steps per
    action must be an integer.

    Args:
        f: The desired frequency.

    Returns:
        The closest possible frequency.
    """
    targets = 100 / np.arange(1, 401)
    if f in targets:
        adjusted = f
    else:
        adjusted = targets[np.argmin(np.abs(targets - f))]
        msg = f"Desired frequency {f} gets adjusted to {adjusted}"
        warnings.warn(msg)
    return adjusted
