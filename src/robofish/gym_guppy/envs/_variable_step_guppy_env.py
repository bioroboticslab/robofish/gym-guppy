import abc
from typing import Any, List, Optional

from deprecated import deprecated
import numpy as np

from ._goal_guppy_env import GoalGuppyEnv
from ._guppy_env import GuppyEnv


@deprecated(
    version="0.4.0",
    reason=(
        "This class is deprecated and will be removed in a future version. "
        "Use `ConfigurableGuppyEnv` instead."
    ),
)
class VariableStepGuppyEnv(GuppyEnv, abc.ABC):
    """Abstract base class for envs that use variable step length."""

    def __init__(
        self,
        *,
        min_steps_per_action: int = 0,
        max_steps_per_action: Optional[int] = None,
        **kwargs: Any,
    ) -> None:
        super().__init__(**kwargs)

        self._min_steps_per_action = min_steps_per_action
        self._max_steps_per_action = max_steps_per_action
        self.enable_step_logging = False

    @property
    def _max_steps_per_action_reached(self) -> bool:
        if self._max_steps_per_action is None:
            return False
        return self._action_steps >= self._max_steps_per_action  # type: ignore

    @property
    def _min_steps_per_action_performed(self) -> bool:
        return self._action_steps >= self._min_steps_per_action  # type: ignore

    def _update_log(self, action: np.ndarray, **kwargs) -> None:
        super()._update_log(action=action, **kwargs)
        self._info["action_completed"].append(self.robot.action_completed())
        self._info["action_steps"].append(self._action_steps)

    @property
    def _internal_sim_loop_condition(self) -> bool:
        return not (
            self._min_steps_per_action_performed
            and (self.robot.action_completed() or self._max_steps_per_action_reached)
        )

    def _get_info_keys(self) -> List[str]:
        keys = super()._get_info_keys()
        keys += ["action_completed", "action_steps"]
        return keys


class VariableStepGoalGuppyEnv(VariableStepGuppyEnv, GoalGuppyEnv, abc.ABC):
    """Environment for using both `VariableStepGuppyEnv` and `GoalGuppyEnv`."""

    @property
    def _internal_sim_loop_condition(self) -> bool:
        return super()._internal_sim_loop_condition and not self.goal_reached()
