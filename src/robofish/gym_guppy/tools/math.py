"""Raycasting, pose conversion etc."""
import numpy as np
from numba import njit

from robofish.core import (
    entity_boundary_distance,
    entity_entities_minimum_sector_distances,
)

from deprecated import deprecated

__pdoc__ = {
    "orientation_vector_to_rad": False,
    "normalize": False,
    "rotation": False,
    "row_norm": False,
    "is_point_left": False,
    "get_local_poses": False,
    "transform_sin_cos": False,
    "polar_coordinates": False,
}


def _distances_to_raycast(distances, max_dist):
    return 1 - np.clip(distances / max_dist, 0, 1)


@deprecated(
    version="0.4.0",
    reason="Legacy raycasting is deprecated use `robofish.core` raycasts instead.",
)
def orientation_vector_to_rad(pose):
    return np.float32([pose[0], pose[1], np.arctan2(pose[3], pose[2])])


def rad_to_orientation_vector(pose: np.ndarray) -> np.ndarray:
    """Convert a pose from [x, y, theta] to [x, y, cos(theta), sin(theta)].

    Args:
        pose (np.ndarray): Pose in [x, y, theta] format.

    Returns:
        np.ndarray: Pose in [x, y, cos(theta), sin(theta)] format.
    """
    return np.float32([pose[0], pose[1], np.cos(pose[2]), np.sin(pose[2])])


@deprecated(
    version="0.4.0",
    reason="Legacy raycasting is deprecated use `robofish.core` raycasts instead.",
)
def normalize(x: np.ndarray):
    return x / np.sqrt((x**2).sum())


@deprecated(
    version="0.4.0",
    reason="Legacy raycasting is deprecated use `robofish.core` raycasts instead.",
)
@njit(fastmath=True)
def rotation(alpha):
    c, s = np.cos(alpha), np.sin(alpha)
    return np.array(((c, -s), (s, c)))


@deprecated(
    version="0.4.0",
    reason="Legacy raycasting is deprecated use `robofish.core` raycasts instead.",
)
@njit(fastmath=True)
def row_norm(matrix: np.ndarray):
    return np.sqrt(np.sum(matrix**2, axis=1))


@deprecated(
    version="0.4.0",
    reason="Legacy raycasting is deprecated use `robofish.core` raycasts instead.",
)
@njit(fastmath=True)
def is_point_left(a, b, c):
    """Compute if c is left of the line ab.

    Args:
        a (np.ndarray): Point a.
        b (np.ndarray): Point b.
        c (np.ndarray): Point c.

    Returns:
        bool: True if c is left of the line ab and False otherwise.
    """
    return (b[0] - a[0]) * (c[1] - a[1]) - (b[1] - a[1]) * (c[0] - a[0]) > 0


@deprecated(
    version="0.4.0",
    reason="Legacy raycasting is deprecated use `robofish.core` raycasts instead.",
)
@njit(fastmath=True)
def get_local_poses(poses, relative_to):
    local_poses = poses - relative_to
    R = rotation(relative_to[2])
    local_poses[:, :2] = local_poses[:, :2].dot(R)

    return local_poses


@deprecated(
    version="0.4.0",
    reason="Legacy raycasting is deprecated use `robofish.core` raycasts instead.",
)
@njit(fastmath=True)
def transform_sin_cos(radians):
    rads = np.atleast_2d(radians).reshape(-1, 1)
    return np.concatenate((np.sin(rads), np.cos(rads)), axis=-1)


@deprecated(
    version="0.4.0",
    reason="Legacy raycasting is deprecated use `robofish.core` raycasts instead.",
)
@njit(fastmath=True)
def polar_coordinates(points):
    # compute polar coordinates
    pts = np.atleast_2d(points)
    dist = row_norm(pts)
    phi = np.arctan2(pts[:, 1], pts[:, 0])

    return dist, phi


def ray_casting_walls(fish_pose, world_bounds, ray_orientations, max_dist):
    """Compute distances to walls (raycast).

    Args:
        fish_pose (np.ndarray): Pose of the observer.
        world_bounds (np.ndarray): World bounds.
        ray_orientations (np.ndarray): Ray orientations.
        max_dist (float): Maximum distance.

    Returns:
        np.ndarray: Distances to walls.
    """
    return _distances_to_raycast(
        entity_boundary_distance(
            observer_pose=rad_to_orientation_vector(fish_pose),
            boundary=world_bounds,
            ray_angles=ray_orientations,
        ),
        max_dist=max_dist,
    )


def compute_dist_bins(relative_to, poses, bin_boundaries, max_dist):
    """Compute distances to other entities (raycast).

    Args:
        relative_to (np.ndarray): Pose of the observer.
        poses (np.ndarray): Poses of the other entities.
        bin_boundaries (np.ndarray): Bin boundaries.
        max_dist (float): Maximum distance.

    Returns:
        np.ndarray: Distances to other entities.
    """
    assert poses.shape[1] == 2 or poses.shape[1] == 3
    # ignore orientation of other poses
    poses = np.c_[poses[:, :2], np.zeros((poses.shape[0], 2))]
    poses[:, 2] = 1

    return _distances_to_raycast(
        entity_entities_minimum_sector_distances(
            entity_poses=np.concatenate(
                (
                    rad_to_orientation_vector(relative_to).reshape(1, -1),
                    rad_to_orientation_vector(poses.T).T,
                )
            ),
            observer_index=0,
            sector_limit_angles=bin_boundaries,
        ),
        max_dist=max_dist,
    )


def sigmoid(x, shrink):
    """Sigmoid function.

    Used by the PID controller.

    Args:
        x (np.ndarray): Input.
        shrink (float): Shrink factor.
    """
    return 2.0 / (1 + np.exp(-x * shrink)) - 1.0
