"""Base classes for fish and robots that are related to physics simulation.

The most relevant things to look at in this module are the getter methods that
fish and robots inherit from `Body`, e.g. `Body.get_pose`, `Body.get_position`,
`Body.get_orientation` and the setters, e.g. `Body.set_angular_velocity`,
`Body.set_linear_velocity`, `Body.set_orientation`, `Body.set_pose` and
`Body.set_position`.

Note: Bodies are set to only collide with walls, so there are no collisions
between moving bodies. The shape and size of the body only matters for wall
collisions and for legacy rendering.

Note: The Box2D world is scaled by 100.0, so all positions and velocities are
*internally* in cm and cm/s. The getters and setters convert to and from meters
and meters/s. So e.g., `body._body.position` is in cm and `body.get_position()`
converts this to meters.
"""
import abc
from typing import Optional, Tuple

import Box2D
import numpy as np

from deprecated import deprecated

__pdoc__ = {
    "Body.color": False,
    "Body.highlight_color": False,
    "Body.draw": False,
    "Body.plot": False,
    "Circle.draw": False,
    "Circle.plot": False,
    "FishBody.plot_vertices": False,
    "Polygon.draw": False,
    "Polygon.plot_vertices": False,
    "Quad.draw": False,
    "Quad.plot": False,
    "Triangle.plot_vertices": False,
}

_world_scale = 100.0


class Body:
    """Base class for all bodies."""

    _density = 1.0
    _friction = 0.01
    _restitution = 0.0

    _linear_damping = 0.00  # * _world_scale
    _angular_damping = 0.00  # * _world_scale

    def __init__(
        self,
        world: Box2D.b2World,
        position: Optional[np.ndarray] = None,
        orientation: Optional[float] = None,
    ):
        """Initialize a new body.

        Args:
            world (Box2d.b2World): The Box2D world to add the body to.
            position (np.ndarray): The position of the body in meters.
            orientation (float): The orientation of the body in radians.
        """
        if self.__class__ == Body:
            raise NotImplementedError("Abstract class Body cannot be instantiated.")
        self._color = np.array((93, 133, 195))
        self._highlight_color = np.array((238, 80, 62))

        if position is None:
            position = [0.0, 0.0]
        position = np.asarray(position)

        if orientation is None:
            orientation = 0.0

        if np.isnan(position).all() and np.isnan(orientation).all():
            raise ValueError(
                "Attemted initializing body with nan pose. "
                "This might happen if a guppy class does not "
                "support setting it's own pose"
            )

        self._world = world
        self._body: Box2D.b2Body = world.CreateDynamicBody(
            position=Box2D.b2Vec2(*(_world_scale * position)),
            angle=orientation,
            linearDamping=self._linear_damping,
            angularDamping=self._angular_damping,
        )
        self._body.linearVelocity = Box2D.b2Vec2(*[0.0, 0.0])
        self._body.angularVelocity = 0.0

    @property
    def width(self):
        """Get the width of the body in meters."""
        raise NotImplementedError

    @property
    def height(self):
        """Get the height of the body in meters."""
        raise NotImplementedError

    def __del__(self):
        """Destroy the body when it is deleted."""
        try:
            self._world.DestroyBody(self._body)
        except AttributeError:
            pass

    def get_position(self) -> np.ndarray:
        """Get the position of the body in meters.

        Returns:
            np.ndarray: position in meters
        """
        return np.asarray(self._body.position) / _world_scale

    def set_position(self, position: np.ndarray):
        """Set the position of the body in meters.

        Args:
            position (np.ndarray): position in meters
        """
        # Box2D can not handle np.float32
        self._body.position = (position * _world_scale).astype(np.float64)

    def get_orientation(self):
        """Get the orientation angle in radians.

        Returns:
            float: orientation angle in radians (unbounded)
        """
        return self._body.angle

    def set_orientation(self, orientation):
        """Set the orientation angle in radians.

        Args:
            orientation (float): orientation angle in radians (unbounded)
        """
        # Box2D can not handle np.float32
        self._body.angle = float(orientation)

    def get_pose(self):
        """Get the pose of the body, a composition of position and orientation.

        Returns:
            x (float): x position in meters
            y (float): y position in meters
            orientation (float): orientation angle in radians (unbounded)
        """
        position = np.asarray(self._body.position) / _world_scale
        return tuple((*position, self._body.angle))

    def set_pose(self, pose):
        """Set the pose of the body, a composition of position and orientation.

        Index 0 and 1 are the x and y position in meters, index 2 is the
        orientation angle in radians (unbounded).

        Args:
            pose: pose of the body
        """
        self.set_position(pose[:2])
        self.set_orientation(pose[2])

    def set_linear_velocity(self, linear_velocity: np.ndarray, local: bool = False):
        """Set the linear velocity of the body in meters per second.

        Args:
            linear_velocity (np.ndarray): linear velocity in meters per second
            local (bool): if True, the velocity is interpreted as a relative vector
        """
        if local:
            linear_velocity = self._body.GetWorldVector(np.asarray(linear_velocity))
        self._body.linearVelocity = _world_scale * Box2D.b2Vec2(linear_velocity)

    def get_linear_velocity(self):
        """Get the linear velocity of the body in meters per second."""
        return self._body.linearVelocity / _world_scale

    def set_angular_velocity(self, angular_velocity):
        """Set the angular velocity of the body in radians per second.

        Args:
            angular_velocity (float): angular velocity in radians per second
        """
        self._body.angularVelocity = angular_velocity

    def get_angular_velocity(self):
        """Get the angular velocity of the body in radians per second."""
        return self._body.angularVelocity

    def get_state(self):
        """Get the state of the body, which is the same as the pose."""
        return self.get_pose()

    def get_local_point(self, point):
        """Get the local point relative to the body's origin given a world point.

        Args:
            point (np.ndarray): The point in absoulte world coordinates.
        """
        return (
            np.asarray(
                self._body.GetLocalPoint(
                    _world_scale * np.asarray(point, dtype=np.float64)
                ),
                dtype=np.float32,
            )
            / _world_scale
        )

    def get_world_point(self, point):
        """Get the world point given a local point relative to the body's origin.

        Args:
            point (np.ndarray): The point in local body coordinates.
        """
        return (
            np.asarray(
                self._body.GetWorldPoint(
                    _world_scale * np.asarray(point, dtype=np.float64)
                ),
                dtype=np.float32,
            )
            / _world_scale
        )

    def get_global_point(self, point):
        """Get the world point given a local point relative to the body's origin.

        Alias for get_world_point.

        Args:
            point (np.ndarray): The point in local body coordinates.
        """
        return self.get_world_point(point)

    def get_local_vector(self, vector):
        """Get the local vector given a world vector.

        Args:
            vector (np.ndarray): The vector in absolute world coordinates.
        """
        return np.asarray(self._body.GetLocalVector(vector))

    def get_world_vector(self, vector):
        """Get the world vector given a local vector.

        Args:
            vector (np.ndarray): The vector in local body coordinates.
        """
        return np.asarray(self._body.GetWorldVector(vector))

    def get_global_vector(self, vector):
        """Get the world vector given a local vector.

        Alias for get_world_vector.

        Args:
            vector (np.ndarray): The vector in local body coordinates.
        """
        return self.get_world_vector(vector)

    def get_local_orientation(self, angle: float) -> float:
        """Get the local angle given a world angle.

        Args:
            angle (float): The angle in absolute world coordinates in radians.
        """
        return angle - self._body.angle

    def get_world_orientation(self, angle: float) -> float:
        """Get the world angle given a local angle.

        Args:
            angle (float): The angle in local body coordinates in radians.
        """
        return self._body.angle + angle

    def get_global_orientation(self, angle: float) -> float:
        """Get the world angle given a local angle.

        Alias for get_world_orientation.

        Args:
            angle (float): The angle in local body coordinates in radians.
        """
        return self.get_world_orientation(angle)

    def get_local_pose(self, pose: float) -> Tuple[float, float, float]:
        """Get the local pose given a world pose.

        Args:
            pose (float): The pose in absolute world coordinates.
        """
        return tuple(
            (*self.get_local_point(pose[:2]), self.get_local_orientation(pose[2]))
        )

    def get_world_pose(self, pose) -> Tuple[float, float, float]:
        """Get the world pose given a local pose.

        Args:
            pose: The pose in local body coordinates.
        """
        return tuple(
            (*self.get_world_point(pose[:2]), self.get_world_orientation(pose[2]))
        )

    def get_global_pose(self, pose):
        """Get the world pose given a local pose.

        Alias for get_world_pose.

        Args:
            pose: The pose in local body coordinates.
        """
        return self.get_world_pose(pose)

    def collides_with(self, other):
        """Check if the body is in contact with another body.

        Args:
            other (Body): The other body to check for contact with.
        """
        for contact_edge in self._body.contacts_gen:
            if contact_edge.other == other and contact_edge.contact.touching:
                return True

    @property
    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def color(self):
        """Get the color of the body."""
        return self._color

    @color.setter
    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def color(self, color):
        """Set the color of the body."""
        color = np.asarray(color, dtype=np.int32)
        color = np.maximum(color, np.zeros_like(color, dtype=np.int32))
        color = np.minimum(color, np.full_like(color, 255, dtype=np.int32))
        self._color = color

    @property
    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def highlight_color(self):
        """Get the highlight color of the body."""
        return self._highlight_color

    @highlight_color.setter
    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def highlight_color(self, color):
        """Set the highlight color of the body."""
        color = np.asarray(color, dtype=np.int32)
        color = np.maximum(color, np.zeros_like(color, dtype=np.int32))
        color = np.minimum(color, np.full_like(color, 255, dtype=np.int32))
        self._highlight_color = color

    @abc.abstractmethod
    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def draw(self, viewer):
        """Draw the body in the viewer."""
        raise NotImplementedError(
            "The draw method needs to be implemented by the subclass of Body."
        )

    @abc.abstractmethod
    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def plot(self, axes, **kwargs):
        """Plot the body in the axes."""
        raise NotImplementedError(
            "The plot method needs to be implemented by the subclass of Body."
        )


@deprecated(
    version="0.4.0",
    reason="Rendering is deprecated, use `robofish-io-render` instead.",
)
class Quad(Body):
    """A rectangular body."""

    def __init__(self, width, height, **kwargs):
        """Initialize the quad."""
        super().__init__(**kwargs)

        self._width = width
        self._height = height

        self._fixture = self._body.CreatePolygonFixture(
            box=Box2D.b2Vec2(
                self._width / 2 * _world_scale, self._height / 2 * _world_scale
            ),
            density=self._density,
            friction=self._friction,
            restitution=self._restitution,
            # radius=.000001
        )

    @property
    def width(self):
        """Get the width of the quad."""
        return self._width

    @property
    def height(self):
        """Get the height of the quad."""
        return self._height

    @property
    def vertices(self):
        """Get the vertices of the quad in world coordinates."""
        return (
            np.asarray(
                [[self._body.GetWorldPoint(v) for v in self._fixture.shape.vertices]]
            )
            / _world_scale
        )

    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def draw(self, viewer):
        """Draw the quad in the viewer."""
        viewer.draw_polygon(self.vertices[0], filled=True, color=self._color)

    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def plot(self, axes, **kwargs):
        """Plot the quad in the axes."""
        from robofish.gym_guppy.tools.plotting import plot_rect

        return plot_rect(axes, self, **kwargs)

    def get_width(self):
        """Get the width of the quad."""
        return self._width

    def get_height(self):
        """Get the height of the quad."""
        return self._height


class Circle(Body):
    """A circular body."""

    def __init__(self, radius, **kwargs):
        """Initialize the circle."""
        super().__init__(**kwargs)

        self._radius = radius

        self._fixture = self._body.CreateCircleFixture(
            radius=self._radius * _world_scale,
            density=self._density,
            friction=self._friction,
            restitution=self._restitution,
        )

    @property
    def width(self):
        """Get the width of the circle."""
        return 2 * self._radius

    @property
    def height(self):
        """Get the height of the circle."""
        return 2 * self._radius

    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def draw(self, viewer):
        """Draw the circle in the viewer."""
        viewer.draw_aacircle(
            position=self.get_position(), radius=self._radius, color=self._color
        )

    @property
    def vertices(self):
        """Get the vertices of the circle in world coordinates."""
        return np.array([[self.get_position()]])

    def get_radius(self):
        """Get the radius of the circle."""
        return self._radius

    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def plot(self, axes, **kwargs):
        """Plot the circle in the axes."""
        from robofish.gym_guppy.tools.plotting import plot_circle

        return plot_circle(axes, self, **kwargs)


class Polygon(Body):
    """A polygon body."""

    def __init__(self, width: float, height: float, **kwargs):
        """Initialize the polygon."""
        super().__init__(**kwargs)

        self._width = width
        self._height = height

        # TODO: right now this assumes that all subpolygons have the same
        # number of edges
        # TODO: rewrite such that arbitrary subpolygons can be used here
        vertices = self._shape_vertices()

        v_size = np.amax(vertices, (0, 1)) - np.amin(vertices, (0, 1))
        vertices /= v_size
        vertices *= np.array((width, height))

        centroid = np.zeros(2)
        area = 0.0
        for vs in vertices:
            # compute centroid of polygon
            a = 0.5 * np.abs(
                np.dot(vs[:, 0], np.roll(vs[:, 1], 1))
                - np.dot(vs[:, 1], np.roll(vs[:, 0], 1))
            )
            area += a
            centroid += vs.mean(axis=0) * a
        centroid /= area

        self.__local_vertices = vertices - centroid
        self.__local_vertices.setflags(write=False)

        for v in self.__local_vertices:
            self._body.CreatePolygonFixture(
                shape=Box2D.b2PolygonShape(vertices=(v * _world_scale).tolist()),
                density=self._density,
                friction=self._friction,
                restitution=self._restitution,
                # radius=.00000001
            )

        self._fixtures = self._body.fixtures

    @property
    def width(self):
        """Get the width of the polygon."""
        return self._width

    @property
    def height(self):
        """Get the height of the polygon."""
        return self._height

    @property
    def vertices(self):
        """Get the vertices of the polygon in world coordinates."""
        return np.array(
            [
                [self.get_world_point(v) for v in vertices]
                for vertices in self.__local_vertices
            ]
        )

    @property
    def local_vertices(self):
        """Get the vertices of the polygon in local coordinates."""
        return self.__local_vertices

    @property
    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def plot_vertices(self):
        """Plot the polygon."""
        raise NotImplementedError

    @staticmethod
    def _shape_vertices() -> np.ndarray:
        raise NotImplementedError

    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def draw(self, viewer):
        """Draw the polygon in the viewer."""
        for vertices in self.vertices:
            viewer.draw_polygon(vertices, filled=True, color=self._color)

    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def plot(self, axes, **kwargs):
        """Plot the polygon in the axes."""
        from robofish.gym_guppy.tools.plotting import plot_polygon

        return plot_polygon(axes, self, **kwargs)


class Triangle(Polygon):
    """A triangle shaped body."""
    @staticmethod
    def _shape_vertices():
        """Get the vertices of the triangle in local coordinates."""
        return np.array([[(-0.5, 0.0), (0.0, 0.0), (0.0, 1.0)]])

    @property
    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def plot_vertices(self):
        """Get the vertices for plotting the triangle."""
        return self.vertices.reshape((-1, 2))


class FishBody(Polygon):
    """A body shaped like a fish."""

    def __init__(self, length, width, height=None, **kwargs):
        """Initialize the fish body."""
        super().__init__(width=length, height=width, **kwargs)

    @property
    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated, use `robofish-io-render` instead.",
    )
    def plot_vertices(self):
        """Get the vertices for plotting the fish body."""
        return self.vertices.reshape((-1, 2))

    @staticmethod
    def _shape_vertices() -> np.ndarray:
        return np.array(
            [
                [
                    (+3.0, +0.0),
                    (+2.5, +1.0),
                    (+1.5, +1.5),
                    (-2.5, +1.0),
                    (-4.5, +0.0),
                    (-2.5, -1.0),
                    (+1.5, -1.5),
                    (+2.5, -1.0),
                ]
            ]
        )
