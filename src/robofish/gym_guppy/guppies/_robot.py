import abc
from typing import Any

import gymnasium as gym
import numpy as np
from scipy.spatial import cKDTree

from robofish.gym_guppy.guppies import Agent, TurnBoostAgent, TurnSpeedAgent
from robofish.gym_guppy.guppies._base_agents import VelocityControlledAgent


class Robot(Agent, abc.ABC):
    """Base class for robots."""

    _linear_damping = 0.0
    _angular_damping = 0.0

    def __init__(self, **kwargs: Any) -> None:
        """Initialize the robot."""
        super().__init__(**kwargs)
        self._env_state = None
        self._env_kd_tree = None

        self._color = np.array((165, 98, 98))

    def set_env_state(self, env_state: np.ndarray, kd_tree: cKDTree) -> None:
        """Set the environment state and kd tree.

        Args:
            env_state: The environment state.
            kd_tree: The kd tree.
        """
        self._env_state = env_state
        self._env_kd_tree = kd_tree

    @property
    @abc.abstractmethod
    def action_space(self) -> gym.spaces.Box:
        """Get the action space of the robot."""
        pass

    @abc.abstractmethod
    def set_action(self, action: np.ndarray) -> None:
        """Set a new action for the robot to execute."""
        pass

    @abc.abstractmethod
    def action_completed(self) -> bool:
        """Check if the current action is completed."""
        pass


class TurnSpeedRobot(Robot, TurnSpeedAgent):
    """Simple robot that turns by a given angle and moves forward with a given speed.

    This robot does not use a PID controller, so it should not be used to train
    a policy that should be executed later on a physical robot.
    The intended use case is to train a policy that should later be used as a
    fish model, e.g. with SQIL.
    Use the guppy counterpart TurnSpeedGuppy after training the policy.

    If you want to move the robot forward by a distance, you may calculate the
    velocity like this:
        v = d / t
    Where t = 1 / robot_actions_per_second.

    This robot is not intended to be used with VariableStepGuppyEnv.
    """

    def action_completed(self) -> bool:
        """Check if the current action is completed.

        Always true for this robot.
        """
        return True

    @property
    def action_space(self) -> gym.spaces.Box:
        """Get the action space of the robot.

        The action space is a box with the following bounds:
            [-max_turn, max_turn] for the turn angle,
            [0.0, max_speed] for the speed.

        Returns:
            The action space.
        """
        return gym.spaces.Box(
            low=np.float32([-self._max_turn, 0.0]),
            high=np.float32([self._max_turn, self._max_speed]),
        )

    def set_action(self, action: np.ndarray) -> None:
        """Set a new action for the robot to execute.

        The action must be two-dimensional and have the following format: [turn, speed].

        Args:
            action: The action to execute.
        """
        self.turn = action[0]
        self.speed = action[1]


class TurnBoostRobot(Robot, TurnBoostAgent):
    """Robot that turns by a given angle and "boosts" with a given amount."""

    def set_action(self, action: np.ndarray) -> None:
        """Set a new action for the robot to execute.

        The action must be two-dimensional and have the following format: [turn, boost].

        Args:
            action: The action to execute.
        """
        self.turn = action[0]
        self.boost = action[1]

    @property
    def action_space(self) -> gym.spaces.Box:
        """Get the action space of the robot.

        The action space is a box with the following bounds:
            [-max_turn, max_turn] for the turn angle,
            [0.0, max_boost] for the boost amount.
        """
        return gym.spaces.Box(
            low=np.float32([-self._max_turn, 0.0]),
            high=np.float32([self._max_turn, self._max_boost]),
        )

    def action_completed(self) -> bool:
        """Check if the current action is completed.

        Always true for this robot.
        """
        return True


class GoToRobot(Robot, VelocityControlledAgent):
    """Robot that moves to a given target position."""

    _linear_damping = 0.0  # * _world_scale
    _angular_damping = 0.0  # * _world_scale

    @property
    def action_space(self) -> gym.spaces.Box:
        """Get the action space of the robot.

        The action space is a box with the following bounds:
            [-0.05, -0.05] for the x and y position of the target in meters.

        Note that the target position is relative to the robot's position.
        """
        return gym.spaces.Box(
            low=np.float32([-0.05, -0.05]), high=np.float32([0.05, 0.05])
        )

    def set_action(self, action: np.ndarray) -> None:
        """Set a new action for the robot to execute.

        The action must be two-dimensional and have the following format: [x, y].
        Note that the target position is relative to the robot's position.
        """
        self._target = self.get_world_point(action)


class GlobalTargetRobot(Robot, VelocityControlledAgent):
    """Robot that moves to a given target position."""

    def __init__(self, modulated: bool = False, **kwargs: Any) -> None:
        """Initialize the robot.

        Args:
            modulated: If true, the robot can also modulate its speed.
            **kwargs: Additional arguments.
        """
        super(GlobalTargetRobot, self).__init__(**kwargs)
        # set position epsilon to 2cm
        self._pos_eps = 0.02
        self._modulated = modulated

        if self._modulated:
            self._action_space = gym.spaces.Box(
                low=np.r_[self._world_bounds[0], 0.0],
                high=np.r_[self._world_bounds[1], 0.2],
                dtype=np.float32,
            )
        else:
            self._action_space = gym.spaces.Box(
                low=self._world_bounds[0],
                high=self._world_bounds[1],
                dtype=np.float32,
            )

    @property
    def action_space(self) -> gym.spaces.Box:
        """Get the action space of the robot.

        The action space is a box with the following bounds:
            [world_bounds[0], world_bounds[1]] for the x and y target in meters.
            [0.0, 0.2] for the speed if modulated is true.
        """
        return self._action_space

    def set_action(self, action: np.ndarray) -> None:
        """Set a new action for the robot to execute.

        The action must be two-dimensional and have the following format: [x, y].
        If modulated is true, the action must be three-dimensional and have the
        following format: [x, y, speed].
        x and y are the absolute target position in meters.
        """
        if self._modulated:
            self.two_wheels_controller.fwd_ctrl.speed = action[-1]
            self._target = action[:-1]
        else:
            self._target = action

    def action_completed(self) -> bool:
        """Return true if the robot is close enough to the target position."""
        pos_error: float = np.linalg.norm(self._target - self.get_position())
        return pos_error < self._pos_eps


class PolarCoordinateTargetRobot(GlobalTargetRobot):
    """Robot that moves to a given target position in polar coordinates."""

    def __init__(self, **kwargs: Any) -> None:
        """Initialize the robot."""
        super().__init__(**kwargs)

        self._action_space = gym.spaces.Box(
            low=np.float32((-np.pi, 0.0)),
            # TODO this was initially set to .3
            high=np.float32((np.pi, 0.15)),
        )

    def set_action(self, action: np.ndarray) -> None:
        """Set a new action for the robot to execute.

        The action must be two-dimensional and have the following format:
        [angle, distance].
        (angle in radians and distance in meters.)

        This angle and distance are often referred to as turn and forward.

        Args:
            action (np.ndarray): The action to execute.
        """
        local_target = np.array((np.cos(action[0]), np.sin(action[0]))) * action[1]
        super(PolarCoordinateTargetRobot, self).set_action(
            self.get_global_point(local_target)
        )


class TurnRobot(PolarCoordinateTargetRobot):
    """Robot that turns to a given angle.

    This robot's forward speed is not controlled by the action.
    However, the forward speed is not constant and depends on the robot's PID
    controller.
    """

    def __init__(self, forward: float = 0.15, **kwargs: Any) -> None:
        """Initialize the robot.

        Args:
            forward: The distance the robot will move forward.
            **kwargs: Additional arguments.
        """
        super().__init__(**kwargs)

        self.forward = forward
        self._action_space = gym.spaces.Box(
            low=np.float32((-np.pi,)),
            high=np.float32((np.pi,)),
        )

    def set_action(self, action: np.ndarray) -> None:
        """Set a new action for the robot to execute.

        The action must be one-dimensional and have the following format: [angle].
        angle is the angle in radians.
        """
        super().set_action(np.float32([action[0], self.forward]))
