import abc
from typing import Any, List

from deprecated import deprecated
import numpy as np
from scipy.spatial import cKDTree

from robofish.gym_guppy.guppies._base_agents import (
    Agent,
    TorqueThrustAgent,
    TurnBoostAgent,
    TurnSpeedAgent,
)


class Guppy(Agent, abc.ABC):
    """Abstract base class for guppies."""

    def __init__(self, robot_poses=None, id=None, *args, **kwargs):
        """Initialize guppy."""
        super().__init__(*args, **kwargs)

    @abc.abstractmethod
    def compute_next_action(self, state: List[Agent], kd_tree: cKDTree = None):
        """Compute next action.

        This method is called by the environment in the `step` method.

        Args:
            state: List of all agents in the environment.
            kd_tree: KDTree of all agents in the environment.
        """
        raise NotImplementedError


class DummyGuppy(Guppy):
    """Dummy guppy for rollouts with live guppy."""

    def compute_next_action(self, state: List[Agent], kd_tree: cKDTree = None):
        """Compute next action.

        Does nothing.
        """
        pass

    def step(self, time_step):
        """Step the guppy.

        Does nothing.
        """
        pass


@deprecated(version="0.4.0", reason="Use `fish_models` instead.")
class TorqueGuppy(Guppy, TorqueThrustAgent):
    """Guppy that always applies a constant torque.

    Deprecated: Use `fish_models` instead.
    """

    def compute_next_action(self, **kwargs):
        self._torque = 250


@deprecated(version="0.4.0", reason="Use `fish_models` instead.")
class BoostGuppy(Guppy, TorqueThrustAgent):
    """Guppy that always applies a constant thrust.

    Deprecated: Use `fish_models` instead.
    """

    def compute_next_action(self, state: List["Agent"], kd_tree: cKDTree = None):
        print(str(self._body.mass) + " " + str(self._body.linearVelocity))
        if np.linalg.norm(self._body.linearVelocity) < 0.05:
            self._thrust = 10.0


@deprecated(version="0.4.0", reason="Use `fish_models` instead.")
class RandomTurnBoostGuppy(Guppy, TurnBoostAgent):
    """Guppy that applies random turn and boost.

    Deprecated: Use `fish_models` instead.
    """

    def __init__(
        self, max_turn: float = 0.75, max_boost: float = 0.3, *args: Any, **kwargs: Any
    ) -> None:
        super().__init__(*args, **kwargs)
        self.__max_turn = max_turn
        self.__max_boost = max_boost

    def compute_next_action(self, state: List["Agent"], kd_tree: cKDTree = None):
        self.turn = np.random.randn() * self.__max_turn
        self.boost = np.abs(np.random.randn() * self.__max_boost)


@deprecated(version="0.4.0", reason="Use `fish_models` instead.")
class TurnBoostGuppy(Guppy, TurnBoostAgent, abc.ABC):
    """Deprecated: Use `fish_models` instead."""

    pass


class TurnSpeedGuppy(Guppy, TurnSpeedAgent, abc.ABC):
    """Simple guppy that is intended as a parent class for learned fish models.

    What needs to be implemented is the method `compute_next_action`,
    in which self.turn and self.speed need to be set.

    If you want to move the robot forward by a distance, you may
    calculate the velocity like this:
        v = d / t
    Where t = 1 / guppy_actions_per_second.
    """

    _linear_damping = 0.0
    _angular_damping = 0.0
