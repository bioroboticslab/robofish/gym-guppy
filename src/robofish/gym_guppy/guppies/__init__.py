"""Guppies and robots."""
# isort:skip_file
from ._base_agents import (
    Agent,
    TurnBoostAgent,
    TorqueThrustAgent,
    ConstantVelocityAgent,
    TurnSpeedAgent,
    VelocityControlledAgent,
)
from ._guppy import (
    Guppy,
    DummyGuppy,
    TurnSpeedGuppy,
    TurnBoostGuppy,
    TorqueGuppy,
    BoostGuppy,
    RandomTurnBoostGuppy,
)
from ._robot import (
    Robot,
    TurnSpeedRobot,
    TurnBoostRobot,
    GoToRobot,
    GlobalTargetRobot,
    PolarCoordinateTargetRobot,
    TurnRobot,
)
from ._couzin_guppies import (
    BaseCouzinGuppy,
    ClassicCouzinGuppy,
    BoostCouzinGuppy,
    AdaptiveCouzinGuppy,
    BiasedAdaptiveCouzinGuppy,
)
from ._adaptive_agent import AdaptiveAgent
from ._perturbed_guppies import PerturbedAdaptiveCouzinGuppy

__all__ = [
    "Agent",
    "TurnBoostAgent",
    "ConstantVelocityAgent",
    "TurnSpeedAgent",
    "VelocityControlledAgent",
    "Guppy",
    "DummyGuppy",
    "TurnSpeedGuppy",
    "TurnBoostGuppy",
    "BaseCouzinGuppy",
    "ClassicCouzinGuppy",
    "BoostCouzinGuppy",
    "AdaptiveCouzinGuppy",
    "BiasedAdaptiveCouzinGuppy",
    "PerturbedAdaptiveCouzinGuppy",
    "Robot",
    "TurnSpeedRobot",
    "TurnBoostRobot",
    "GlobalTargetRobot",
    "PolarCoordinateTargetRobot",
    "TurnRobot",
    "GoToRobot",
    "TorqueGuppy",
    "TorqueThrustAgent",
    "AdaptiveAgent",
    "BoostGuppy",
    "RandomTurnBoostGuppy",
]
__pdoc__ = {
    "TurnBoostGuppy": False,
    "BaseCouzinGuppy": False,
    "ClassicCouzinGuppy": False,
    "BoostCouzinGuppy": False,
    "AdaptiveCouzinGuppy": False,
    "BiasedAdaptiveCouzinGuppy": False,
    "PerturbedAdaptiveCouzinGuppy": False,
    "TurnBoostRobot": False,
    "TorqueGuppy": False,
    "TorqueThrustAgent": False,
    "AdaptiveAgent": False,
    "BoostGuppy": False,
    "RandomTurnBoostGuppy": False,
    "Agent.set_color": False,
}
