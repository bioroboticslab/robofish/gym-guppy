"""Base classes for all agents."""
import abc
from collections import deque
from typing import Optional
import warnings

from deprecated import deprecated
import numpy as np
from Box2D import b2Vec2

from robofish.gym_guppy.bodies import FishBody
from robofish.gym_guppy.tools.controller import TwoWheelsController


class Agent(FishBody, abc.ABC):
    """Base class for all agents."""

    _max_linear_velocity = 0.2  # meters / s
    _max_angular_velocity = 0.5 * np.pi  # radians / s

    # adjust these parameters
    _density = 0.00025
    _friction = 0.0
    _restitution = 0.0

    _linear_damping = 15.0  # * _world_scale
    _angular_damping = 60.0  # * _world_scale

    def __init__(self, *, world_bounds, **kwargs):
        """Initialize the agent.

        Args:
            world_bounds: The bounds of the world in meters.
        """
        # all parameters in real world units
        super().__init__(length=0.02, width=0.004, **kwargs)
        self._body.bullet = True

        self._world_bounds = np.asarray(world_bounds)

        # will be set by the environment
        self._id = None

        # put all guppies into same group (negative so that no collisions are detected)
        self._fixtures[0].filterData.groupIndex = -1

        self._color = np.array((133, 133, 133))
        self._highlight_color = (255, 255, 255)

    @property
    def id(self):
        """Get the id of the agent."""
        return self._id

    @id.setter
    def id(self, id):
        """Set the id of the agent.

        Args:
            id: The id of the agent.

        Raises:
            Warning: If the id was already set.
        """
        if self._id is not None:
            warnings.warn(f"Agent id was changed from {self._id} to {id}")
        self._id = id

    @deprecated(
        version="0.4.0",
        reason="Rendering is deprecated. Use `robofish-io-render` instead.",
    )
    def set_color(self, color):
        """Set the color of the agent."""
        self._highlight_color = color

    @abc.abstractmethod
    def step(self, time_step: float) -> None:
        """Perform a step of the agent.

        This method is called by the environment.
        It should be implemented by the agent.

        Args:
            time_step (float): The time step in seconds.

        Returns:
            The reward for the step.
        """
        pass


class TorqueThrustAgent(Agent, abc.ABC):
    """Base class for agents that can apply torque and thrust."""

    # TODO add action space
    def __init__(self, **kwargs):
        """Initialize the agent."""
        super().__init__(**kwargs)
        self._max_torque = 0.3
        self._max_thrust = 10.0

        self._angular_vel_threshold = 0.2
        self._linear_vel_threshold = 0.03

        self._torque = None
        self._thrust = None

    def step(self, time_step: float) -> None:
        """Perform a step of the agent.

        This method is called by the environment.

        Args:
            time_step (float): The time step in seconds.
        """
        print(str(self._body.linearVelocity) + " - " + str(self._body.angularVelocity))

        if (
            self._torque
            and np.linalg.norm(self._body.angularVelocity) < self._angular_vel_threshold
        ):
            # self._body.ApplyTorque(self._torque, wake=True)
            self._body.ApplyAngularImpulse(self._torque, wake=True)
            self._torque = None
        elif (
            self._thrust
            and np.linalg.norm(self._body.angularVelocity) < self._angular_vel_threshold
        ):
            self._body.ApplyLinearImpulse(
                self._body.GetWorldVector(b2Vec2(self._thrust, 0.0)),
                point=self._body.worldCenter,
                wake=True,
            )
            self._thrust = None
        elif (
            np.linalg.norm(self._body.linearVelocity) < self._linear_vel_threshold
            and np.linalg.norm(self._body.angularVelocity) < self._angular_vel_threshold
        ):
            # random movement
            if np.random.rand() >= 0.5:
                thrust = np.random.rand() * self._max_thrust
                self._body.ApplyLinearImpulse(
                    self._body.GetWorldVector(b2Vec2(thrust, 0.0)),
                    point=self._body.worldCenter,
                    wake=True,
                )
            else:
                torque = (np.random.rand() - 0.5) * self._max_torque
                # self._body.ApplyTorque(torque, wake=True)
                self._body.ApplyAngularImpulse(torque, wake=True)


class TurnBoostAgent(Agent, abc.ABC):
    """Base class for agents that can turn and boost.

    The TurnBoostGuppy approximates the behaviour observed with real fish,
    by first turning into a desired direction and then performing a forward boost.

    The TurnBoostGuppy can be controlled by setting the turn and boost properties.
    """

    def __init__(self, **kwargs):
        """Initialize the agent."""
        super().__init__(**kwargs)

        self._max_turn_per_step = np.pi / 4.0
        self._max_boost_per_step = 0.01
        self._max_turn = 4 * self._max_turn_per_step
        self._max_boost = 2 * self._max_boost_per_step

        self.__turn = None
        self.__boost = None
        self.__reset_velocity = False

    @property
    def turn(self):
        """Get the turn of the agent."""
        return self.__turn

    @turn.setter
    def turn(self, turn):
        """Set the turn of the agent.

        Args:
            turn (float): The turn of the agent in radians.
        """
        self.__reset_velocity = True
        self.__turn = np.minimum(np.maximum(turn, -self._max_turn), self._max_turn)

    @property
    def boost(self):
        """Get the boost of the agent."""
        return self.__boost

    @boost.setter
    def boost(self, boost):
        """Set the boost of the agent.

        Args:
            boost (float): The boost of the agent.
        """
        self.__reset_velocity = True
        self.__boost = np.minimum(np.maximum(boost, 0.0), self._max_boost)

    def step(self, time_step: float) -> None:
        """Perform a step of the agent.

        This method is called by the environment.

        Args:
            time_step (float): The time step in seconds. (unused)
        """
        if self.__reset_velocity:
            self._body.linearVelocity = b2Vec2(0.0, 0.0)
            self.__reset_velocity = False
        if self.turn:
            t = np.minimum(
                np.maximum(self.turn, -self._max_turn_per_step), self._max_turn_per_step
            )
            self._body.angle += t
            self.turn -= t
        elif self.boost:
            b = np.minimum(self.__boost, self._max_boost_per_step)
            self._body.ApplyLinearImpulse(
                self._body.GetWorldVector(b2Vec2(b, 0.0)),
                point=self._body.worldCenter,
                wake=True,
            )
            self.__boost -= b


class ConstantVelocityAgent(Agent, abc.ABC):
    """Base class for agents that move with constant velocity."""

    _linear_damping = 0.0
    _angular_damping = 0.0

    def __init__(self, **kwargs):
        """Initialize the agent."""
        super().__init__(**kwargs)

        self._velocity = 0.2

        self._turn = None
        self._max_turn = np.pi / 10.0

    def step(self, time_step):
        """Perform a step of the agent.

        This method is called by the environment.

        Args:
            time_step (float): The time step in seconds. (unused)
        """
        if self._turn:
            t = np.minimum(np.maximum(self._turn, -self._max_turn), self._max_turn)
            self._body.angle += t
            self._turn -= t

        # wall collisions may cause angular velocity, set back to 0
        self.set_angular_velocity(0.0)
        self.set_linear_velocity((self._velocity, 0.0), local=True)


class TurnSpeedAgent(Agent, abc.ABC):
    """Base class for agents that can turn and move with adjustable speed."""

    def __init__(self, max_turn=np.pi, max_speed=1.0, **kwargs):
        """Initialize the agent."""
        super().__init__(**kwargs)

        self._max_turn = max_turn
        self._max_speed = max_speed

        self.__turn = None
        self.__speed = None

    @property
    def turn(self):
        """Get the turn of the agent."""
        return self.__turn

    @turn.setter
    def turn(self, turn: float):
        """Set the turn of the agent.

        Args:
            turn (float): The turn of the agent in radians.
        """
        self.__turn = np.minimum(np.maximum(turn, -self._max_turn), self._max_turn)

    @property
    def speed(self):
        """Get the speed of the agent."""
        return self.__speed

    @speed.setter
    def speed(self, speed: float):
        """Set the speed of the agent.

        Args:
            speed (float): The speed of the agent.
        """
        self.__speed = np.minimum(np.maximum(speed, -self._max_speed), self._max_speed)

    def step(self, time_step: float) -> None:
        """Perform a step of the agent.

        This method is called by the environment.

        Args:
            time_step (float): The time step in seconds. (unused)
        """
        if self.turn:
            self._body.angle += self.turn
            self.__turn = None
        if self.speed is not None:
            # wall collisions may cause angular velocity, set back to 0
            self.set_angular_velocity(0.0)
            self.set_linear_velocity([self.speed, 0.0], local=True)
            self.__speed = None


class VelocityControlledAgent(Agent, abc.ABC):
    """Base class for agents that can be controlled by velocity.

    This agent is used by the robot that include a simulation of the PID controllers.
    """

    def __init__(self, ctrl_params=None, **kwargs):
        """Initialize the agent."""
        super().__init__(**kwargs)

        if ctrl_params is None:
            ctrl_params = {}

        self._two_wheels_controller = TwoWheelsController(**ctrl_params)
        self._target = None
        self._counter = 0

        # TODO: add explanation for magic numbers
        self._tau = 4
        self._vel_buffer = deque([(0.0, 0.0)] * self._tau, maxlen=self._tau)
        self._ctrl_buffer = deque([(0.0, 0.0)] * self._tau, maxlen=self._tau)
        self._pose_tau = 1
        self._pose_buffer = deque(
            [(0.0, 0.0, 0.0)] * self._pose_tau, maxlen=self._pose_tau
        )
        self._target_buffer = deque(
            [np.array((0.0, 0.0))] * self._pose_tau, maxlen=self._pose_tau
        )

    @property
    def target(self) -> Optional[np.ndarray]:
        """Get the current target of the agent."""
        return self._target

    @property
    def two_wheels_controller(self) -> TwoWheelsController:
        """Get the two wheels controller of the agent."""
        return self._two_wheels_controller

    def step(self, time_step: float) -> None:
        """Perform a step of the agent.

        This method is called by the environment.

        Args:
            time_step (float): The time step in seconds. (unused)
        """
        # run controller with 25Hz (simulation with 100Hz)
        if self._counter % 4 == 0:
            self._pose_buffer.append(self.get_pose())
            self._target_buffer.append(self._target)
            motor_speeds = self._two_wheels_controller.speeds(
                self._pose_buffer[0], self._target_buffer[0]
            )
            pwm_commands = motor_speeds.vel_to_pwm()
            self._ctrl_buffer.append(pwm_commands.pwm_to_vel().get_local_velocities())

            v_x_tau, v_r_tau = self._vel_buffer[0]
            c_x_tau, c_r_tau = self._ctrl_buffer[0]
            # x_vel, r_vel = np.mean(np.asarray(self._vel_buffer)[0, :], axis=0)
            # x_vel, r_vel = self._vel_buffer.popleft()
            x_vel = 0.975 * self._vel_buffer[-1][0] + 0.2 * (c_x_tau - v_x_tau)
            r_vel = 0.96 * self._vel_buffer[-1][1] + 0.2 * (c_r_tau - v_r_tau)

            self._vel_buffer.append((x_vel, r_vel))

            self.set_angular_velocity(r_vel)
            self.set_linear_velocity([x_vel, 0.0], local=True)
        self._counter += 1
