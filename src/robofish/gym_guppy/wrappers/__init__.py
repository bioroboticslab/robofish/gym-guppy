"""Wrappers for the Robofish environment."""
from .action_wrapper import (
    ClipToActionSpaceWrapper,
    DiscreteActionWrapper,
    FlatActionWrapper,
    Local2GlobalWrapper,
    MovementLimitWrapper,
    NormalizeActionWrapper,
)
from .evaluation_wrapper import EvaluationWrapper, OmniscienceWrapper
from .observation_wrapper import (
    FeedbackInspectionWrapper,
    FlatObservationsWrapper,
    FrameStack,
    GoalObservationWrapper,
    IgnorePastWallsWrapper,
    IndexedFrameStack,
    LocalGoalObservationWrapper,
    LocalObservationsWrapper,
    LocalPolarCoordinateGoalObservationWrapper,
    LocalPolarCoordinateObservations,
    RayCastingGoalWrapper,
    RayCastingWrapper,
    TimeWrapper,
    TimeWrapper2,
    TrackAdaptiveZones,
)

__all__ = [
    "DiscreteActionWrapper",
    "FlatActionWrapper",
    "MovementLimitWrapper",
    "NormalizeActionWrapper",
    "Local2GlobalWrapper",
    "ClipToActionSpaceWrapper",
    "LocalObservationsWrapper",
    "RayCastingWrapper",
    "FrameStack",
    "IndexedFrameStack",
    "IgnorePastWallsWrapper",
    "TrackAdaptiveZones",
    "TimeWrapper",
    "TimeWrapper2",
    "RayCastingGoalWrapper",
    "FlatObservationsWrapper",
    "GoalObservationWrapper",
    "LocalGoalObservationWrapper",
    "LocalPolarCoordinateObservations",
    "LocalPolarCoordinateGoalObservationWrapper",
    "FeedbackInspectionWrapper",
    "EvaluationWrapper",
    "OmniscienceWrapper",
]

__pdoc__ = {
    "EvaluationWrapper": False,
    "OmniscienceWrapper": False,
}