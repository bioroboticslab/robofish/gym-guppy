"""Evaluation wrappers for the Guppy Environment."""
import os

import gymnasium as gym
import h5py
import numpy as np

from robofish.gym_guppy import AdaptiveCouzinGuppy
from robofish.gym_guppy.guppies import PerturbedAdaptiveCouzinGuppy
from robofish.gym_guppy.reward.robot_reward import follow_reward

from deprecated import deprecated


@deprecated(
    version="0.4.0",
    reason="This works only with the deprecated Couzin guppies. "
    "Use `robofish-io` for storing hdf5 files and do evaluation later.",
)
class EvaluationWrapper(gym.Wrapper):
    """Evaluation wrapper for the Guppy Environment."""

    def __init__(self, env, saving_dir, name, num_episodes):
        """Initialize the evaluation wrapper."""
        gym.Wrapper.__init__(self, env)
        self.f = h5py.File(os.path.join(saving_dir, "{}.hdf5".format(name)), "w")
        self.episode_counter = 0
        self.num_episodes = num_episodes

    def reset(self):
        """Reset the environment and return the initial observation."""
        if self.episode_counter == self.num_episodes:
            print("closing")
            self.f.close()
            return
        self.episode_counter += 1

        return_value = self.env.reset()
        self.step_counter = 0

        self.last_state = self.env.get_state()

        self.group = self.f.create_group("episode_{}".format(self.episode_counter))
        self.group.create_dataset(
            "rewards", (self.env.spec.max_episode_steps,), dtype=np.float32
        )
        self.group.create_dataset(
            "follow_metric", (self.env.spec.max_episode_steps,), dtype=np.float32
        )
        self.group.create_dataset(
            "distance_to_fish", (self.env.spec.max_episode_steps,), dtype=np.float32
        )
        self.group.create_dataset(
            "zones_size", (self.env.spec.max_episode_steps, 3), dtype=np.float32
        )
        guppy = next(self.env.guppies)
        self.group.attrs["adaptive_zone_factor"] = guppy._adaptive_zone_factors[0]
        self.group.attrs["zone_radius"] = guppy._zone_radius
        self.group.attrs["grow_factor"] = guppy._adaptive_zone_grow_factor
        self.group.attrs["shrink_factor"] = guppy._adaptive_zone_shrink_factor
        self.group.attrs["bias_gain"] = guppy.bias_gain
        self.group.attrs["zoo_factor"] = guppy._zoo_factor

        return return_value

    def step(self, action):
        state, reward, done, info = self.env.step(action)

        self.group["rewards"][self.step_counter] = reward

        current_state = self.env.get_state()
        self.group["follow_metric"][self.step_counter] = follow_reward(
            current_state, self.last_state
        )
        self.last_state = current_state

        # TODO: use kd_tree instead
        self.group["distance_to_fish"][self.step_counter] = distance_to_fish(
            current_state
        )

        guppy = next(self.env.guppies)
        self.group["zones_size"][self.step_counter] = np.array(
            guppy.couzin_zones, dtype=np.float32
        ).flatten()
        self.step_counter += 1

        return state, reward, done, info


@deprecated(
    version="0.4.0",
    reason="This wrapper is closely linked to the deprecated Couzin guppies "
    "and an old implementation of an actor critic."
)
class OmniscienceWrapper(gym.Wrapper):
    """Wrapper that provides observations with all information about the environment.

    This wrapper provides the following observations:
    - visible_for_all: The observations that are visible for all agents
    - only_for_critic: The observations that are only visible for the critic

    The observations that are only visible for the critic are:
    - The distance to the fish
    - The couzin zones
    - The time left (not implemented yet)
    - The domain randomization parameters (not implemented yet)
    """
    def __init__(self, env):
        gym.Wrapper.__init__(self, env)
        num_guppies = env.num_guppies
        # TODO: Time left, DR Parameters
        self.observation_space = gym.spaces.Dict(
            dict(
                visible_for_all=env.observation_space,
                only_for_critic=gym.spaces.Box(
                    low=0.0,
                    high=np.inf,
                    shape=(num_guppies * (3 + 6) + 1,),
                    dtype=np.float32,
                ),
            )
        )
        self.max_time_steps = self.env.spec.max_episode_steps

    def reset(self):
        self.t = 0
        state = self.env.reset()
        return dict(visible_for_all=state, only_for_critic=self._get_information())

    def step(self, action):
        self.t += 1
        state, reward, done, info = self.env.step(action)
        return (
            dict(visible_for_all=state, only_for_critic=self._get_information()),
            reward,
            done,
            info,
        )

    def _get_information(self):
        dr_parameters = np.array(
            [
                g.dr_parameter_list
                for g in self.env.guppies
                if isinstance(g, PerturbedAdaptiveCouzinGuppy)
            ]
        ).flatten()
        zones = np.array(
            [
                g.couzin_zones
                for g in self.env.guppies
                if isinstance(g, AdaptiveCouzinGuppy)
            ]
        ).flatten()
        time_left = np.array(
            [(self.max_time_steps - self.t) / self.max_time_steps]
        ).flatten()
        info = np.concatenate((dr_parameters, zones, time_left), axis=0)
        print(info.shape)
        return info
